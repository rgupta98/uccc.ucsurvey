/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [UCCC]
GO

--drop table dbo.[CustUsers]
--drop table [dbo].[ConnectedPlants]
--drop table dbo.WaterReceivers
--drop table dbo.CustSurveys
--drop table dbo.Customers
--drop table dbo.WaterProviders



/****** Object:  Table [dbo].[InventoryMaster]    Script Date: 24-Dec-17 17:44:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TABLE [dbo].[WaterProviders](
	[WaterProviderId] [int] IDENTITY(1,1) NOT NULL,
	[ProviderName] [varchar](200) NOT NULL,
	[Logo] [varbinary](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[WaterProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customers](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](200) NOT NULL,
	[WaterProviderId] int CONSTRAINT Customer_WaterProviderId FOREIGN KEY REFERENCES dbo.WaterProviders(WaterProviderId) NOT NULL,
	[CCId] varchar(200) NOT NULL,
	[AccountId] int NOT NULL,
	[WaterMeterNumber] int NOT NULL,
	[Diameter] int NOT NULL,
	[ServiceClass] varchar(100) NOT NULL,
	[ConnectionStatus] varchar(100) NULL,
	[ServiceAddrStreetNumber] varchar(200) NULL,
	[ServiceAddrStreetNumberSuffix] varchar(50) NULL,
	[ServiceAddrStreetDirPrefix] varchar(50) NULL,
	[ServiceAddrStreetName] varchar(300) NULL,
	[ServiceAddrStreetType] varchar(200) NULL,
	[ServiceAddrStreetDirSuffix] varchar(50) NULL,
	[ServiceAddrCityName] varchar(100) NULL,
	[ServiceAddrState] varchar(100) NULL,
	[ServiceAddrZipCode] varchar(50) NULL,
	[BillingAddrStreetNumber] varchar(200) NULL,
	[BillingAddrStreetNumberSuffix] varchar(50) NULL,
	[BillingAddrStreetDirPrefix] varchar(50) NULL,
	[BillingAddrStreetName] varchar(300) NULL,
	[BillingAddrStreetType] varchar(200) NULL,
	[BillingAddrStreetDirSuffix] varchar(50) NULL,
	[BillingAddrAptDesc] varchar(100) NULL,
	[BillingAddrAptNumber] varchar(50) NULL,
	[BillingAddrCityName] varchar(100) NULL,
	[BillingAddrState] varchar(100) NULL,
	[BillingAddrZipCode] varchar(50) NULL,	
	[OwnerPlaceOfWork] varchar(200) NULL,
	[OwnerFirstName] varchar(200) NULL,
	[OwnerLastName] varchar(200) NULL,
	[OwnerAddrStreetNumber] varchar(200) NULL,
	[OwnerAddrStreetNumberSuffix] varchar(50) NULL,
	[OwnerAddrStreetDirPrefix] varchar(50) NULL,
	[OwnerAddrStreetName] varchar(300) NULL,
	[OwnerAddrStreetType] varchar(200) NULL,
	[OwnerAddrStreetDirSuffix] varchar(50) NULL,
	[OwnerAddrAptDesc] varchar(100) NULL,
	[OwnerAddrAptNumber] varchar(50) NULL,
	[OwnerAddrCityName] varchar(100) NULL,
	[OwnerAddrState] varchar(100) NULL,
	[OwnerAddrZipCode] varchar(50) NULL,
	[DueDate] datetime NULL 	
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[CustUsers](
	[CustUserId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] varchar(300) NULL,
	[LastName] varchar(300) NULL,
	[Phone] varchar(100) NULL,
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](200) NOT NULL,
	[UserType] varchar(50) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CustSurveys](
	[CustSurveyId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] CONSTRAINT CustSurvey_CustomerId FOREIGN KEY REFERENCES dbo.Customers(CustomerId) NOT NULL,
	[ServiceClass] [varchar](200) NULL,
	[Diameter] [int] NOT NULL,
	[PropertyType] varchar(100) NULL,
	[PropertyTypeValue] varchar(200) NULL,
	[BackflowPreventionDevice] varchar(10) NULL,
	[OtherWaterUsingEquipment] varchar(10) NULL,
	[OtherWaterUsingEquipmentDesc] varchar(4000) NULL,
	[SurveyOwnerFullName] varchar (500) NULL,
	[SurveyDate] datetime NULL,
	[SurveyOwnerPhone] varchar (100) NULL,
	[ContactEmailAddress] varchar(200) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustSurveyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[ConnectedPlants](
	[ConnectedPlantId] [int] IDENTITY(1,1) NOT NULL,
	[CustSurveyId] [int] CONSTRAINT ConnectedPlants_CustSurvey FOREIGN KEY REFERENCES dbo.[CustSurveys](CustSurveyId) NOT NULL,
	[ConnectedPlantName] varchar(500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ConnectedPlantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WaterReceivers](
	[WaterReceiverId] [int] IDENTITY(1,1) NOT NULL,
	[CustSurveyId] [int] CONSTRAINT WaterReceiver_CustSurvey FOREIGN KEY REFERENCES dbo.[CustSurveys](CustSurveyId) NOT NULL,
	[SupplyReceiver] varchar(500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[WaterReceiverId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO