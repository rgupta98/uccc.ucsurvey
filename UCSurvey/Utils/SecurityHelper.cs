using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace UCSurvey.Utils
{
  public class HashPassword
  {
    public static string GetHash(string plain)
    {
      // SHA256 is disposable by inheritance.  
      using (var sha256 = SHA256.Create())
      {
        // Send a sample text to hash.  
        var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(plain));
        // Get the hashed string.  
        var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        return hash;
      }
    }

  }
}
