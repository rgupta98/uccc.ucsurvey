import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AlertModule } from 'ngx-bootstrap';
import { AsyncLocalStorageModule } from 'angular-async-local-storage';
import { TextMaskModule } from 'angular2-text-mask';
import { NgDatepickerModule } from 'ng2-datepicker';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BootstrapModalModule } from 'ngx-bootstrap-modal';
import { ConditionallyValidateModule } from 'ng-conditionally-validate';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TypeaheadModule } from 'ngx-bootstrap';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './services/auth.guard';
import { HomeGuard } from './services/home.guard';
import { UserService } from './services/user.service'
import { CustomerResolve } from './services/customer-resolve.service'
import { HeaderComponent } from './components/header/header.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FooterComponent } from './components/footer/footer.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { ProviderImageComponent } from './components/provider-image/provider-image.component';
import { SelectCustomerComponent } from './components/select-customer/select-customer.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { ShowErrorsComponent } from './components/show-errors/show-errors.component';
import { TelephoneDirective, PositiveDirective } from './services/telephone.directive';
import { MaxValueDirective } from './services/telephone.directive';
import { EmailDirective } from './services/email.directive';
import { ValidateEqualDirective } from './services/validate-equal.directive';
import { AddCustomerComponent } from './components/add-customer/add-customer.component';
import { EditCustomerComponent } from './components/edit-customer/edit-customer.component';
import { CustNavComponent } from './components/cust-nav/cust-nav.component';
import { ToastOptions } from 'ng2-toastr';

export class CustomOption extends ToastOptions {
  animate = 'flyRight'; // you can override any options available
  showCloseButton = true;
  positionClass = 'toast-top-left';
}
const appRoutes: Routes = [
  {
    path: 'home',
    canActivate: [AuthGuard, HomeGuard],
    component: HomeComponent,
    resolve: {
      customer: CustomerResolve
    }
  },
  {
    path: 'cust',
    canActivate: [AuthGuard],
    component: SelectCustomerComponent,
  },
  {
    path: 'editCust',
    canActivate: [AuthGuard],
    component: EditCustomerComponent,
  },
  {
    path: 'eCust',
    canActivate: [AuthGuard],
    component: EditCustomerComponent,
  },
  {
    path: 'logout',
    canActivate: [AuthGuard],
    component: EditCustomerComponent,
  },
  {
    path: 'addCust',
    canActivate: [AuthGuard],
    component: AddCustomerComponent,
    resolve: {
      customer: CustomerResolve
    }
  },
  {
    path: 'forgotPassword',
    component: ForgotPasswordComponent,
  },
  {
    path: 'registerUser',
    component: RegisterUserComponent,
  },
  {
    path: '',
    pathMatch: 'prefix',
    component: LoginFormComponent
  },
  {
    path: '**',
    component: NotfoundComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HeaderComponent,
    LoginFormComponent,
    FooterComponent,
    NotfoundComponent,
    HomeComponent,
    ProviderImageComponent,
    SelectCustomerComponent,
    ForgotPasswordComponent,
    ConfirmComponent,
    RegisterUserComponent,
    ShowErrorsComponent,
    TelephoneDirective,
    PositiveDirective,
    MaxValueDirective,
    EmailDirective,
    ValidateEqualDirective,
    AddCustomerComponent,
    EditCustomerComponent,
    CustNavComponent
  ],
  entryComponents: [
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ConditionallyValidateModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AlertModule.forRoot(),
    AsyncLocalStorageModule,
    TextMaskModule,
    NgDatepickerModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    BootstrapModalModule,
    TypeaheadModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [UserService, AuthGuard, HomeGuard, CustomerResolve, { provide: 'BASE_URL', useFactory: getBaseUrl }, { provide: ToastOptions, useClass: CustomOption },],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
