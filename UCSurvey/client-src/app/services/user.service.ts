import { Injectable } from '@angular/core';
import { AsyncLocalStorage } from 'angular-async-local-storage';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class UserService {

  private isUserLoggedIn: boolean;
  public username: string;
  public sessionExists: boolean;

  constructor(public http: Http, protected localStorage: AsyncLocalStorage) {
    this.isUserLoggedIn = false;
   }

  resetUserLoggedOut() {
    this.localStorage.setItem('loggedOut', false).subscribe(() => { });
  }
  setUserLoggedOut() {
    this.localStorage.setItem('loggedOut', true).subscribe(() => { });
  }

  getUserLoggedOut() {
    return Observable.create(observer => {
      this.localStorage.getItem<string>('loggedOut').subscribe(x => {
        var v = x != undefined && x == true;
        observer.next(v);
        observer.complete();
      });
    });

  }

  getUserName() {
    return Observable.create(observer => {
      this.localStorage.getItem<string>('userName').subscribe(x => {
        observer.next(x);
        observer.complete();
      });
    });
  }

  getUserType() {
    return Observable.create(observer => {
      this.localStorage.getItem<string>('userType').subscribe(x => {
        var v = x != undefined && x.length > 2;
        observer.next(x);
        observer.complete();
      });
    });
  }

  isUserUCCC() {
    return Observable.create(observer => {
      this.localStorage.getItem<string>('userType').subscribe(x => {
        var v = x != undefined && x.length > 2 && (x == "\"UCCC Service Rep\"" || x == "UCCC Service Rep");
        observer.next(v);
        observer.complete();
      });
    });
  }

  setUserLoggedIn() {
      this.isUserLoggedIn = true;
      return this.localStorage.setItem('loggedIn', this.isUserLoggedIn);
    }

  getCustomerSelected() {
    return Observable.create(observer => {
      this.localStorage.getItem<string>('ccIdSet').subscribe(x => {
        var v = x != undefined && x.length > 2;
        observer.next(v);
        observer.complete();
      });
    });
    
  }
  setCustomerSelected(ccId: string) {
    return this.localStorage.setItem('ccIdSet', ccId);
  }

  resetSelectedCustomer() {
    this.localStorage.removeItem('ccIdSet').subscribe(() => { });
  }

  resetLocalStorage() {
    this.isUserLoggedIn = false;
    this.localStorage.removeItem('loggedIn').subscribe(() => { });
    this.localStorage.removeItem('email').subscribe(() => { });
    this.localStorage.removeItem('password').subscribe(() => { });
    this.localStorage.removeItem('ccIdSet').subscribe(() => { });
  }
  getUserLoggedIn() {

    return Observable.create(observer => {
      this.http.get("api/Login/getSession").subscribe(data => {
        this.sessionExists = data.json();
        if (!this.sessionExists) {
          this.resetLocalStorage();
          observer.next(this.sessionExists);
          observer.complete();
        }
        else {
          this.localStorage.getItem<string>('loggedIn').subscribe(v => {
            this.isUserLoggedIn = v;
            observer.next(v);
            observer.complete();
          })
        }
      });
    });

  }
}
