import { Directive, Attribute } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl, ValidationErrors } from '@angular/forms';


@Directive({
  selector: '[telephoneNumber]',
  providers: [{ provide: NG_VALIDATORS, useExisting: TelephoneDirective, multi: true }]
})
export class TelephoneDirective implements Validator {

  validate(c: FormControl): ValidationErrors {
    const isValidPhoneNumber = /^[(]{0,1}[0-9]{3}[)\.\- ]{0,1}[ ]{0,1}[0-9]{3}[\.\- ]{0,1}[0-9]{4}$/.test(c.value);
    const isValid = isValidPhoneNumber ? isValidPhoneNumber : /^\d{3,3}\d{3,3}\d{4,4}$/.test(c.value);
    const finalVal = c.value == undefined || c.value == null || c.value.length <= 0 || isValid;
    const message = {
      'telephoneNumber': {
        'message': 'Phone is invalid (XXX-XXX-XXXX, where X is a digit)'
      }
    };
    return finalVal ? null : message;
  }
}


@Directive({
  selector: '[maxVal]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MaxValueDirective, multi: true }]
})
export class MaxValueDirective implements Validator {

  constructor( @Attribute('max') public max: number) { }

  validate(c: FormControl): ValidationErrors {
    const upper = this.max > 0 ? this.max : 999999999999999;
    const isValid = c.value == undefined || c.value == null || c.value.length <= 0 || parseFloat(c.value) <= upper;
    const message = {
      'maxVal': {
        'message': 'Value is too large'
      }
    };
    return isValid ? null : message;
  }
}

@Directive({
  selector: '[positive]',
  providers: [{ provide: NG_VALIDATORS, useExisting: PositiveDirective, multi: true }]
})
export class PositiveDirective implements Validator {

  validate(c: FormControl): ValidationErrors {
    const isValid = c.value == undefined || c.value == null || c.value.length <= 0 || /^[0-9]*$/.test(c.value);
    const message = {
      'maxVal': {
        'message': 'positive numbers only'
      }
    };
    return isValid ? null : message;
  }
}
