import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, FormControl, ValidationErrors } from '@angular/forms';


@Directive({
  selector: '[email2]',
  providers: [{ provide: NG_VALIDATORS, useExisting: EmailDirective, multi: true }]
})
export class EmailDirective implements Validator {
  validate(c: FormControl): ValidationErrors {
    return EmailDirective.email2(c);
  }

  static email2(c: FormControl): ValidationErrors {
    const isValid = c.value == undefined || c.value == null || c.value.length <= 0 || /^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-z]{2,4})$/.test(c.value);
    const message = {
      'email2': {
        'message': 'Invalid format. Should be user@domain.com'
      }
    };
    return isValid ? null : message;
  }
}
