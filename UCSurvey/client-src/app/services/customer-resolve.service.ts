import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Component, OnInit, Input, Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class CustomerResolve implements Resolve<any> {
  public bseUrl: string = "";
  public Customer: Customer = null;
  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.getCustomer(route.params['customerId']);
  }

  getCustomer(id: number) {
    return Observable.create(observer => {
      if (id != undefined && id > 0) {
        this.http.get("api/Provider/getCustomer2/" + id).subscribe(data => {
          this.Customer = data.json();
          observer.next(this.Customer);
          observer.complete();
        });
      }
      else {
        this.http.get("api/Provider/getCustomer/1").subscribe(data => {
          this.Customer = data.json();
          observer.next(this.Customer);
          observer.complete();
        });
      }

    });
  }

}

export interface Customer {
  customerId: number;
  accountId: number;
  waterMeterNumber: number;
  cCId: string;
  serviceClass: string;
  diameter: number;
} 
