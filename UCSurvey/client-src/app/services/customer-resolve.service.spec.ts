import { TestBed, inject } from '@angular/core/testing';

import { CustomerResolve } from './customer-resolve.service';

describe('CustomerResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerResolve]
    });
  });

  it('should be created', inject([CustomerResolve], (service: CustomerResolve) => {
    expect(service).toBeTruthy();
  }));
});
