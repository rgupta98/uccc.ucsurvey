import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Injectable()
export class HomeGuard implements CanActivate {

  constructor(private user: UserService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    var val = this.user.getCustomerSelected();
    val.subscribe(x => {
      if (!x)
        this.router.navigate(["cust"]);
    })
    return val;
  }
}
