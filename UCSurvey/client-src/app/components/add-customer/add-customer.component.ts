import { Component, OnInit, Input, Inject, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ProviderImageComponent } from "../provider-image/provider-image.component"
import { UserService } from '../../services/user.service';
import { NgDatepickerComponent } from "ng2-datepicker"
import { DatepickerOptions } from "ng2-datepicker"
import * as enLocale from 'date-fns/locale/en';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConditionallyValidateService } from 'ng-conditionally-validate';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['../../../assets/css/index.css']
})
export class AddCustomerComponent implements OnInit {
  public bseUrl: string = "";
  public doEditCustomer: boolean = false;
  addForm: FormGroup;
  public profileId: number = null;
  public provId: number = null;
  public customerName: string = "";
  public accountId: number;
  public diameter: number;
  public customerCCID: string = "";
  public serviceClass: string = "Commercial";
  public connectionStatus: string = "Active";
  public waterMeterNumber: number;
  public serviceAddrStreetNumber: string = "";
  public serviceAddrStreetNumberSuffix: string = "";
  public serviceAddrStreetDirPrefix: string = "";
  public serviceAddrStreetName: string = "";
  public serviceAddrStreetType: string = "";
  public serviceAddrStreetDirSuffix: string = "";
  public serviceAddrCityName: string = "";
  public serviceAddrState: string = "";
  public serviceAddrZipCode: string = "";

  public billingAddrStreetNumber: string = "";
  public billingAddrStreetNumberSuffix: string = "";
  public billingAddrStreetDirPrefix: string = "";
  public billingAddrStreetName: string = "";
  public billingAddrStreetType: string = "";
  public billingAddrStreetDirSuffix: string = "";
  public billingAddrCityName: string = "";
  public billingAddrState: string = "";
  public billingAddrZipCode: string = "";
  public billingAddrAptDesc: string = "";
  public billingAddrAptNumber: string = "";

  public ownerPlaceOfWork: string = "";
  public ownerFirstName: string = "";
  public ownerLastName: string = "";

  public ownerAddrStreetNumber: string = "";
  public ownerAddrStreetNumberSuffix: string = "";
  public ownerAddrStreetDirPrefix: string = "";
  public ownerAddrStreetName: string = "";
  public ownerAddrStreetType: string = "";
  public ownerAddrStreetDirSuffix: string = "";
  public ownerAddrCityName: string = "";
  public ownerAddrState: string = "";
  public ownerAddrZipCode: string = "";
  public ownerAddrAptDesc: string = "";
  public ownerAddrAptNumber: string = "";
  public duedate: Date = new Date();
  public connectionStatuses: string[] = ["Active", "In Active", "Seasonal Disconnect"];
  public svcClasses: string[] = ["Commercial", "Non Commercial"];

  calOptions: DatepickerOptions = {
    locale: enLocale,
  };

  public oldBillingAddress1: BillingAddress = {
    billingAddrStreetNumber: "",
    billingAddrStreetNumberSuffix: "",
    billingAddrStreetDirPrefix: "",
    billingAddrStreetName: "",
    billingAddrStreetType: "",
    billingAddrStreetDirSuffix: "",
    billingAddrCityName: "",
    billingAddrState: "",
    billingAddrZipCode: "",
    billingAddrAptDesc: "",
    billingAddrAptNumber: "",
  }
  
  public e: CustomerObj = {
    waterProviderId: 1,
    customerName: "",
    serviceClass: "Commercial",
    connectionStatus: "Active",
    diameter:null,
    waterMeterNumber: null,
    accountId: null,
    serviceAddrStreetNumber: "",
    serviceAddrStreetNumberSuffix: "",
    serviceAddrStreetDirPrefix: "",
    serviceAddrStreetName: "",
    serviceAddrStreetType: "",
    serviceAddrStreetDirSuffix: "",
    serviceAddrCityName: "",
    serviceAddrState: "",
    serviceAddrZipCode: "",
    billingAddrStreetNumber: "",
    billingAddrStreetNumberSuffix: "",
    billingAddrStreetDirPrefix: "",
    billingAddrStreetName: "",
    billingAddrStreetType: "",
    billingAddrStreetDirSuffix: "",
    billingAddrCityName: "",
    billingAddrState: "",
    billingAddrZipCode: "",
    billingAddrAptDesc: "",
    billingAddrAptNumber: "",
    ownerAddrStreetNumber: "",
    ownerAddrStreetNumberSuffix: "",
    ownerAddrStreetDirPrefix: "",
    ownerAddrStreetName: "",
    ownerAddrStreetType: "",
    ownerAddrStreetDirSuffix: "",
    ownerAddrCityName: "",
    ownerAddrState: "",
    ownerAddrZipCode: "",
    ownerAddrAptDesc: "",
    ownerAddrAptNumber: "",
    ownerPlaceOfWork: "",
    ownerFirstName: "",
    ownerLastName: "",
    ccId: "",
    doEditCustomer: this.doEditCustomer,
    sameServiceAddress: false,
    oldBillingAddress: this.oldBillingAddress1,
    dueDate: new Date(),
    dueDateString: ""
  };
  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef, private user: UserService, private cv: ConditionallyValidateService, private fb: FormBuilder) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);
    this.addForm = fb.group({
      customerName: "",
      waterMeterNumber: [],
      accountId: [],
      diameter: [],
      serviceClass: "Commercial",
      connectionStatus: "Active",
      serviceAddrStreetNumber: "",
      serviceAddrStreetNumberSuffix: "",
      serviceAddrStreetDirPrefix: "",
      serviceAddrStreetName: "",
      serviceAddrStreetType: "",
      serviceAddrStreetDirSuffix: "",
      serviceAddrCityName: "",
      serviceAddrState: "",
      serviceAddrZipCode: "",
      billingAddrStreetNumber: "",
      billingAddrStreetNumberSuffix: "",
      billingAddrStreetDirPrefix: "",
      billingAddrStreetName: "",
      billingAddrStreetType: "",
      billingAddrStreetDirSuffix: "",
      billingAddrCityName: "",
      billingAddrState: "",
      billingAddrZipCode: "",
      billingAddrAptDesc: "",
      billingAddrAptNumber: "",
      ownerAddrStreetNumber: "",
      ownerAddrStreetNumberSuffix: "",
      ownerAddrStreetDirPrefix: "",
      ownerAddrStreetName: "",
      ownerAddrStreetType: "",
      ownerAddrStreetDirSuffix: "",
      ownerAddrCityName: "",
      ownerAddrState: "",
      ownerAddrZipCode: "",
      ownerAddrAptDesc: "",
      ownerAddrAptNumber: "",
      ownerPlaceOfWork: "",
      ownerFirstName: "",
      ownerLastName: "",
      sameServiceAddress: false,
      dueDate: new Date()
    })
    this.profileId = parseInt(this.route.snapshot.paramMap.get("customerId"));
    this.provId = parseInt(this.route.snapshot.paramMap.get("providerId"));
    if (this.provId > 0) {
      this.e.waterProviderId = this.provId;
    }
  }

  ngOnInit() {

    var custName = this.route.snapshot.paramMap.get("custName");
    if (custName != undefined && custName.length > 2) {
      this.http.get(this.bseUrl + "api/Customer/getCustomerCCID/" + custName).subscribe(r => {
        this.e = r.json();
        this.e.doEditCustomer = true;
        this.e.ccId = custName;
        this.e.oldBillingAddress = this.oldBillingAddress1;
      });
    }
  }

  updateBillingAddress(evt: any) {
    if (evt.target.checked) {
      this.e.oldBillingAddress.billingAddrStreetNumber = this.e.billingAddrStreetNumber;
      this.e.oldBillingAddress.billingAddrStreetNumberSuffix = this.e.billingAddrStreetNumberSuffix;
      this.e.oldBillingAddress.billingAddrStreetDirPrefix = this.e.billingAddrStreetDirPrefix;
      this.e.oldBillingAddress.billingAddrStreetName = this.e.billingAddrStreetName;
      this.e.oldBillingAddress.billingAddrStreetType = this.e.billingAddrStreetType;
      this.e.oldBillingAddress.billingAddrStreetDirSuffix = this.e.billingAddrStreetDirSuffix;
      this.e.oldBillingAddress.billingAddrCityName = this.e.billingAddrCityName;
      this.e.oldBillingAddress.billingAddrState = this.e.billingAddrState;
      this.e.oldBillingAddress.billingAddrZipCode = this.e.billingAddrZipCode;

      this.e.billingAddrStreetNumber = this.e.serviceAddrStreetNumber;
      this.e.billingAddrStreetNumberSuffix = this.e.serviceAddrStreetNumberSuffix;
      this.e.billingAddrStreetDirPrefix = this.e.serviceAddrStreetDirPrefix;
      this.e.billingAddrStreetName = this.e.serviceAddrStreetName;
      this.e.billingAddrStreetType = this.e.serviceAddrStreetType;
      this.e.billingAddrStreetDirSuffix = this.e.serviceAddrStreetDirSuffix;
      this.e.billingAddrCityName = this.e.serviceAddrCityName;
      this.e.billingAddrState = this.e.serviceAddrState;
      this.e.billingAddrZipCode = this.e.serviceAddrZipCode;

    }
    else {
      this.e.billingAddrStreetNumber = this.e.oldBillingAddress.billingAddrStreetNumber;
      this.e.billingAddrStreetNumberSuffix = this.e.oldBillingAddress.billingAddrStreetNumberSuffix;
      this.e.billingAddrStreetDirPrefix = this.e.oldBillingAddress.billingAddrStreetDirPrefix;
      this.e.billingAddrStreetName = this.e.oldBillingAddress.billingAddrStreetName;
      this.e.billingAddrStreetType = this.e.oldBillingAddress.billingAddrStreetType;
      this.e.billingAddrStreetDirSuffix = this.e.oldBillingAddress.billingAddrStreetDirSuffix;
      this.e.billingAddrCityName = this.e.oldBillingAddress.billingAddrCityName;
      this.e.billingAddrState = this.e.oldBillingAddress.billingAddrState;
      this.e.billingAddrZipCode = this.e.oldBillingAddress.billingAddrZipCode;
    }
  }

  onSubmit() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');

    var dueDate = new Date(this.e.dueDate);
    this.e.dueDateString = dueDate.toDateString();
    if (this.e.sameServiceAddress) {
      this.e.billingAddrStreetNumber = this.e.serviceAddrStreetNumber;
      this.e.billingAddrStreetNumberSuffix = this.e.serviceAddrStreetNumberSuffix;
      this.e.billingAddrStreetDirPrefix = this.e.serviceAddrStreetDirPrefix;
      this.e.billingAddrStreetName = this.e.serviceAddrStreetName;
      this.e.billingAddrStreetType = this.e.serviceAddrStreetType;
      this.e.billingAddrStreetDirSuffix = this.e.serviceAddrStreetDirSuffix;
      this.e.billingAddrCityName = this.e.serviceAddrCityName;
      this.e.billingAddrState = this.e.serviceAddrState;
      this.e.billingAddrZipCode = this.e.serviceAddrZipCode;
    }

    if (this.e.doEditCustomer) {
      this.http.post(this.bseUrl + 'api/Customer/editCust', JSON.stringify(this.e),
        { headers: headers }).subscribe(
        response => {
          var r = response.json();
          this.router.navigate(['eCust', {edit: true}]).then(x => {
            this.toastr.success("Customer record was edited successfully with CCID :" + r.ccId, "Customer record edited successfully");
          });

        }, error => {
          if (error.status == 409) {
            this.router.navigate(['cust']).then(x => {
              this.toastr.error('Customer record already exists!', 'Customer Already Exists!', { toastLife: 10000 });
            });
          }
        }
        );
    }
    else {
      this.http.post(this.bseUrl + 'api/Customer/addCust', JSON.stringify(this.e),
        { headers: headers }).subscribe(
        response => {
          var r = response.json();
          this.router.navigate(['cust']).then(x => {
            this.toastr.success("Customer record was added successfully with CCID :" + r.ccId, "Customer record added successfully");
          });

        }, error => {
          if (error.status == 409) {
            this.router.navigate(['cust']).then(x => {
              this.toastr.error('Customer record already exists!', 'Customer Already Exists!', { toastLife: 10000 });
            });
          }
        }
        );
    }
  }
}

export interface CustomerObj {
  waterProviderId: number;
  customerName: string;
  serviceClass: string;
  connectionStatus: string;
  diameter: number;
  accountId: number;
  waterMeterNumber: number;
  serviceAddrStreetNumber: string;
  serviceAddrStreetNumberSuffix: string;
  serviceAddrStreetDirPrefix: string;
  serviceAddrStreetName: string;
  serviceAddrStreetType: string;
  serviceAddrStreetDirSuffix: string;
  serviceAddrCityName: string;
  serviceAddrState: string;
  serviceAddrZipCode: string;
  billingAddrStreetNumber: string;
  billingAddrStreetNumberSuffix: string;
  billingAddrStreetDirPrefix: string;
  billingAddrStreetName: string;
  billingAddrStreetType: string;
  billingAddrStreetDirSuffix: string;
  billingAddrCityName: string;
  billingAddrState: string;
  billingAddrZipCode: string;
  billingAddrAptDesc: string;
  billingAddrAptNumber: string;
  ownerPlaceOfWork: string;
  ownerFirstName: string;
  ownerLastName: string;
  ownerAddrStreetNumber: string;
  ownerAddrStreetNumberSuffix: string;
  ownerAddrStreetDirPrefix: string;
  ownerAddrStreetName: string;
  ownerAddrStreetType: string;
  ownerAddrStreetDirSuffix: string;
  ownerAddrCityName: string;
  ownerAddrState: string;
  ownerAddrZipCode: string;
  ownerAddrAptDesc: string;
  ownerAddrAptNumber: string;
  ccId: string;
  doEditCustomer: boolean;
  sameServiceAddress: boolean;
  oldBillingAddress: BillingAddress;
  dueDate: Date;
  dueDateString: string;
}

export interface BillingAddress {
  billingAddrStreetNumber: string;
  billingAddrStreetNumberSuffix: string;
  billingAddrStreetDirPrefix: string;
  billingAddrStreetName: string;
  billingAddrStreetType: string;
  billingAddrStreetDirSuffix: string;
  billingAddrCityName: string;
  billingAddrState: string;
  billingAddrZipCode: string;
  billingAddrAptDesc: string;
  billingAddrAptNumber: string;
}
