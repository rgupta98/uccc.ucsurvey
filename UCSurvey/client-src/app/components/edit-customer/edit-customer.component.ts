import { Component, OnInit, Input, Inject, ViewContainerRef, Directive, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css', '../../../assets/css/index.css']
})
export class EditCustomerComponent implements OnInit {
  public bseUrl: string = "";
  public customerId: number = null;
  public editCustomer: boolean = true;
  public waterProviders = [];
  public waterProviderId: number;

  public searchCustomers$: Observable<string[]>;
  public selectedCustomer: string = '';

  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef, private user: UserService) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {

    this.user.isUserUCCC().subscribe(x => {
      if (!x) {
        this.router.navigate(['cust']).then(x => { });
      }
    })

    var isLogout = this.route.snapshot.paramMap.get("logout");
    if (isLogout != undefined && isLogout.length > 2 && isLogout == 'true') {
      this.user.resetLocalStorage();
      var toast = this.toastr;
      var url11 = this.bseUrl + "api/Login/logout";
      this.http.get(url11).subscribe(x => {
        this.router.navigate(['cust']).then(x => {
          this.user.setUserLoggedOut();
        });
      });

    }
    else {
      this.http.get(this.bseUrl + "api/Provider/getProviders").subscribe(x => {
        this.waterProviders = x.json();
        this.waterProviderId = this.waterProviders[0].value;
      })
      this.editCustomer = this.route.snapshot.paramMap.get("edit") == 'true';
      this.searchCustomers$ = Observable.create(o => o.next(this.selectedCustomer))
        .mergeMap((srch: string) => this.searchCustomersWith(srch));
    }

  }

  public search: string = '';
  //public url: string = this.bseUrl + "api/Customer/searchCust/";
  public url: string = this.bseUrl + "api/Survey/searchCCID/";

  searchCustomersWith(name) {
    return this.http.get(this.getUrl()).map(x => x.json());
  }

  showNoMatch(e: any) {

  }

  showCustomer(item: any) {
    this.search = item;
  }
  getUrl() {
    return this.url + this.search;
  }

  addCustomer() {
    if (this.waterProviderId <= 0) {
      this.toastr.error("Please select a water provider ", "Water Provider not selected", { toastLife: 10000 });
    }
    else {
      this.router.navigate(['addCust', { providerId: this.waterProviderId }]).then(x => {
      });
    }
  }

  selectCustomer() {

    this.http.get(this.bseUrl + "api/Customer/getCustomerCCID/" + this.search).subscribe(r => {
      var t = r.json();
      this.router.navigate(['addCust', { custName: this.search, customerId: t.customerId }]).then(x => {
        this.toastr.success("Customer record was loaded successfully with name :" + this.search, "Customer selected successfully");
      });
    });
    //this.http.get(this.bseUrl + "api/Customer/getCustomerName/" + this.search).subscribe(r => {
    //  var t = r.json();
    //  this.router.navigate(['addCust', { custName: this.search, customerId: t.customerId }]).then(x => {
    //    this.toastr.success("Customer record was loaded successfully with name :" + this.search, "Customer selected successfully");
    //  });
    //});

  }
}
