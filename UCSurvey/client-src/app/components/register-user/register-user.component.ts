import { Component, OnInit, Input, Inject, ViewContainerRef, Directive, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['../../../assets/css/index.css', './register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  public bseUrl: string = "";
  public invalidPassword: boolean = false;
  public userType: string = "Service Technician";
  public userFirstName: string = "";
  public userLastName: string = "";
  public userEmail: string = "";
  public userPhone: string = "";
  public userPassword: string = "";
  public confirmPassword: string = "";

  //public userTypes: string[] = ["Customer", "Water Provider", "Service Technician", "UCCC Service Rep"];
  public userTypes: string[] = ["Customer", "Water Provider", "Service Technician"];
  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  validatePassword() {
    this.invalidPassword = false;
    if (this.userPassword == undefined || this.userPassword.length < 8)
      this.invalidPassword = true;
    else if (this.confirmPassword == undefined || this.confirmPassword.length < 8)
      this.invalidPassword = true;
    else if (this.userPassword != this.confirmPassword)
      this.invalidPassword = true;
    var letterNumber = /^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z\W_]{8,}$/;
    if (!letterNumber.test(this.userPassword))
      this.invalidPassword = true;
  }

  registerUser() {
    this.validatePassword();
    if (!this.invalidPassword) {
      var model: CustUser = {
        email: this.userEmail,
        phone: this.userPhone,
        password: this.userPassword,
        firstName: this.userFirstName,
        lastName: this.userLastName,
        userType: this.userType
      };
      var headers = new Headers();
      headers.append('Content-Type', 'application/json; charset=utf-8');

      this.http.post(this.bseUrl + 'api/Login/addUser', JSON.stringify(model),
        { headers: headers }).subscribe(
        r => {
          if (r.status == 200) {
            this.router.navigate(['/']).then(x => {
              this.toastr.success("User Added successfully. Please login.", "User Added successfully");
            });
          }
          else {
            this.toastr.error('User with Email already exists!', 'Duplicate user found!', { toastLife: 10000 });
          }
        }, error => {
          if (error.status == 409) {
            this.router.navigate(['cust']).then(x => {
              this.toastr.error('Error Occurrred during adding User!', 'User Add Failed!', { toastLife: 10000 });
            });
          }
        }
        );
    }

  }

}

export interface CustUser {
  email: string;
  password: string;
  phone: string;
  userType: string;
  firstName: string;
  lastName: string;
} 
