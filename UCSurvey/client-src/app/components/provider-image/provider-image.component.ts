import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-provider-image',
  //templateUrl: './provider-image.component.html',
  template: `
    <img [src]="sanitizer.bypassSecurityTrustUrl(imageData)" [width]="imageWidth" [height]="imageHeight"/>
  `,
  styleUrls: ['./provider-image.component.css']
})
export class ProviderImageComponent implements OnInit {
  imageData: any;
  @Input('profileId') profileId: number;
  @Input('provId') provId: number;
  @Input('width') imageWidth: number = 100;
  @Input('height') imageHeight: number = 100;

  constructor(private http: Http,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {

    if (this.provId != undefined && this.provId > 0) {
      this.http.get("api/Provider/getProvImage/" + this.provId)
        .map(image => image.json())
        .subscribe(
        data => {
          this.imageData = 'data:image/png;base64,' + data;
        }
        );
    }
    else {
      this.http.get("api/Provider/getImage/" + this.profileId)
        .map(image => image.json())
        .subscribe(
        data => {
          this.imageData = 'data:image/png;base64,' + data;
        }
        );
    }

  }

}
