import { Component, OnInit, Input, Inject, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConditionallyValidateService } from 'ng-conditionally-validate';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../../../assets/css/index.css','./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public bseUrl: string = "";
  public invalidPhone: boolean = false;
  public invalidPassword: boolean = false;
  public userEmail: string = "";
  public userPhone: string = "";
  public userPassword: string = "";
  public confirmPassword: string = "";

  form22: FormGroup;
  public e: ChangeUserPass = {
    userEmail: "",
    userPhone: "",
    userPassword: "",
    confirmPassword: ""
  };
  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef, private fb: FormBuilder) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);

    this.form22 = fb.group({
      userEmail: '',
      userPhone: '',
      userPassword: '',
      confirmPassword: ''
    })
  }

  ngOnInit() {
    this.e.userEmail = this.route.snapshot.paramMap.get("e");
    var e1 = this.e.userEmail;
    var EMAIL_REGEXP = /^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-z]{2,4})$/;
    if (e1 != undefined && e1.length > 3) {
      var isMatchRegex = EMAIL_REGEXP.test(e1);
      if (!isMatchRegex) {
        this.router.navigate(['/']).then(x => {
          this.toastr.error("Please enter valid email address", "Invalid Email Address", { toastLife: 10000 });
        })
      }
    }
    else {
      this.router.navigate(['/']).then(x => {
        this.toastr.error("Please enter your email address", "Enter Email Address", { toastLife: 10000 });
      })
    }
  }

  validatePhone() {
    var PHONE_REGEXP = /^[(]{0,1}[0-9]{3}[)\.\- ]{0,1}[ ]{0,1}[0-9]{3}[\.\- ]{0,1}[0-9]{4}$/;
    this.invalidPhone = false;
    if (this.e.userPhone == undefined || this.e.userPhone.length < 2) {
      this.invalidPhone = true;
    }
    if (!PHONE_REGEXP.test(this.e.userPhone))
      this.invalidPhone = true;
  }

  validateUserPass() {
    this.invalidPassword = false;
    if (this.e.userPassword == undefined || this.e.userPassword.length < 8)
      this.invalidPassword = true;
    var letterNumber = /^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z\W_]{8,}$/;
    if (!letterNumber.test(this.e.userPassword))
      this.invalidPassword = true;
  }

  validateConfirmPass() {
    this.invalidPassword = false;
    if (this.e.userPassword == undefined || this.e.userPassword.length < 8)
      this.invalidPassword = true;
    else if (this.e.confirmPassword == undefined || this.e.confirmPassword.length < 8)
      this.invalidPassword = true;
  }

  validatePassword() {
    this.invalidPassword = false;
    if (this.e.userPassword == undefined || this.e.userPassword.length < 8)
      this.invalidPassword = true;
    else if (this.e.confirmPassword == undefined || this.e.confirmPassword.length < 8)
      this.invalidPassword = true;
    else if (this.e.userPassword != this.e.confirmPassword)
      this.invalidPassword = true;
    var letterNumber = /^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z\W_]{8,}$/;
    if (!letterNumber.test(this.e.userPassword))
      this.invalidPassword = true;
  }

  changePassword() {
    this.validatePhone();
    this.validatePassword();
    if (!this.invalidPhone && !this.invalidPassword) {
      var model: ChangeUserPass = {
        userEmail: this.e.userEmail,
        userPhone: this.e.userPhone,
        userPassword: this.e.userPassword,
        confirmPassword: this.e.confirmPassword
      };
      var headers = new Headers();
      headers.append('Content-Type', 'application/json; charset=utf-8');

      this.http.post(this.bseUrl + 'api/Login/changePass', JSON.stringify(model),
        { headers: headers }).subscribe(
        r => {
          if (r.status == 200) {
            this.router.navigate(['/']).then(x => {
              this.toastr.success("Password changed successfully. Please login again.", "Password changed successfully");
            });
          }
          else {
            this.toastr.error('Invalid Email or Phone number!', 'Change Password Failed!', { toastLife: 10000 });
          }
        }, error => {
          if (error.status == 409) {
            this.router.navigate(['cust']).then(x => {
              this.toastr.error('Error Occurrred during change password!', 'Change Password Failed!', { toastLife: 10000 });
            });
          }
        }
        );


    }
  }
}

export interface ChangeUserPass {
  userEmail: string;
  userPhone: string;
  userPassword: string;
  confirmPassword: string;
}
