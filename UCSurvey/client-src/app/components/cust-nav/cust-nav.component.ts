import { Component, OnInit, Input, Inject, ViewContainerRef, Directive, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import $ from 'jquery/dist/jquery';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-cust-nav',
  templateUrl: './cust-nav.component.html',
  styleUrls: ['./cust-nav.component.css']
})
export class CustNavComponent implements OnInit {
  public bseUrl: string = "";
  public step: string = "step1";
  public isUserUCCC: boolean = false;
  public userName: string = "";
  public userType: string = "";
  constructor(private router: Router, private user: UserService, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.user.getUserName().subscribe(x => {
      this.userName = x;
    });
    this.user.getUserType().subscribe(x => {
      this.userType = x;
    });

    if (this.router.url === "/cust")
      this.step = "step1";
    if (this.router.url === "/editCust")
      this.step = "step2";
    if (this.router.url === "/eCust;edit=true")
      this.step = "step3";

    this.user.isUserUCCC().subscribe(x => {
      this.isUserUCCC = x;
    })

  }

}
