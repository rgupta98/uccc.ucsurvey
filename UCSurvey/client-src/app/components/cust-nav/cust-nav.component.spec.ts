import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustNavComponent } from './cust-nav.component';

describe('CustNavComponent', () => {
  let component: CustNavComponent;
  let fixture: ComponentFixture<CustNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
