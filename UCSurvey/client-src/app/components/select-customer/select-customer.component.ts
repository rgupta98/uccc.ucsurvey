import { Component, OnInit, Input, Inject, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { UserService } from '../../services/user.service';
import { ConfirmComponent } from '../confirm/confirm.component';
import { DialogService } from "ngx-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

@Component({
  selector: 'app-select-customer',
  templateUrl: './select-customer.component.html',
  styleUrls: ['./select-customer.component.css', '../../../assets/css/index.css']
})
export class SelectCustomerComponent implements OnInit {
  public bseUrl: string = "";
  public customerCCId: string = "";
  
  public searchCCIDs$: Observable<string[]>;
  public selectedCCID: string = '';

  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef, private user: UserService, private dialogService: DialogService) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.user.resetSelectedCustomer();
    this.searchCCIDs$ = Observable.create(o => o.next(this.selectedCCID))
      .mergeMap((srch: string) => this.searchCCIDWith(srch));
  }

  public url: string = this.bseUrl + "api/Survey/searchCCID/";

  searchCCIDWith(name) {
    return "";
    //return this.http.get(this.getUrl()).map(x => x.json());
  }

  showNoMatch(e: any) {

  }

  selectCCID(item: any) {
    this.customerCCId = item;
  }
  getUrl() {
    return this.url + this.customerCCId;
  }

  showConfirm(title1, msg, ccId) {
    let disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: title1,
      message: msg
    }).subscribe((isConfirmed) => {
        //We get dialog result
        if (isConfirmed) {
          this.user.setCustomerSelected(ccId).subscribe(x => {
            this.router.navigate(['home', { ccId: ccId }]);
          });  
        }
        else {
          this.clearSelectedCustomer(ccId);
        }
      });
    //We can close dialog calling disposable.unsubscribe();
    //If dialog was not closed manually close it by timeout
    setTimeout(() => {
      disposable.unsubscribe();
    }, 10000);
  }

  clearSelectedCustomer(ccId) {
    this.http.get("api/Provider/resetCustomer/" + ccId).subscribe(
      response => {
      }, error => {
      }
    );
  }

  searchCustomer() {
    if (this.customerCCId == undefined || this.customerCCId.length < 4) {
      this.toastr.error("Supplied CC Id is invalid", "Invalid CC Id!", { toastLife: 10000 });
      return;
    }

    this.http.get("api/Provider/searchCustomer/" + this.customerCCId).subscribe(
      response => {
        var r = response.json();
        if (r.ccId == undefined || r.ccId == '') {
          this.toastr.error("Supplied CCID# is invalid", "Invalid CCID#!", { toastLife: 10000 });
        }
        else if (r.serviceAddress == "exists") {
          this.toastr.error('Survey for the Meter already exists!', 'Survey Already Exists!', { toastLife: 10000 });
          this.showConfirm('Survey Already Exists!', 'Survey for the CCID# ' + r.ccId + ' already submitted by ' + r.billingAddress +' ! Do you want to continue?', r.ccId);
        }
        else {
          this.user.setCustomerSelected(r.ccId).subscribe(x => {
            this.router.navigate(['home']).then(x => {
              this.toastr.success("Retrieved Survey with CCID# :" + r.ccId, "Survey Retrieved successfully!", { toastLife: 1000 });
            });
          });          
        }
        
      }, error => {
        if (error.status == 409) {
          this.toastr.error('Survey for the Meter already exists!', 'Survey Already Exists!', { toastLife: 10000 });
        }
      }
      );

  }
}
