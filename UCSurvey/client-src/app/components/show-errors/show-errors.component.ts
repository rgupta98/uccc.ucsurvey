// show-errors.component.ts
import { Component, Input, ViewChild } from '@angular/core';
import { AbstractControlDirective, AbstractControl, NgForm } from '@angular/forms';

@Component({
  selector: 'show-errors',
  template: `
   <ul class="fa-ul" [style.width]="getStyle()" *ngIf="shouldShowErrors()">
     <li class="alert-danger asmall2" *ngFor="let error of listOfErrors()"><i class="fa-li fa fa-spinner fa-spin"></i>{{error}}</li>
   </ul>
   <ul class="fa-ul" *ngIf="showEmpty()">
     <li class="alert-danger asmall2">&nbsp;&nbsp;&nbsp;&nbsp;</li>
   </ul>
 `,
  styleUrls: ['../../../assets/css/index.css']
})
export class ShowErrorsComponent {

  private static readonly errorMessages = {
    'required': () => 'This field is required',
    'minlength': (params) => 'The min number of characters is ' + params.requiredLength,
    'maxlength': (params) => 'The max allowed number of characters is ' + params.requiredLength,
    'email': () => 'Invalid format. Should be user@domain.com',
    'email2': (params) => params.message,
    'pattern': (params) => 'The required pattern is: ' + params.requiredPattern,
    'years': (params) => params.message,
    'countryCity': (params) => params.message,
    'uniqueName': (params) => params.message,
    'telephoneNumbers': (params) => params.message,
    'telephoneNumber': (params) => params.message,
    'maxVal': (params) => params.message,
    'validateEqual': () => 'Password mismatch'
  };

  @Input()
  private control: AbstractControlDirective | AbstractControl;

  @Input()
  private ctrl: string;

  @Input()
  private errMessage: string;

  @Input()
  private cssstyle: string;

  @Input()
  private empty: string;

  @Input()
  private myForm: NgForm;

  getControl() {
    if (!this.control && this.ctrl != undefined) {
      this.control = this.myForm.form.get(this.ctrl);
    }
  }

  getStyle() {
    if (this.cssstyle != undefined && this.cssstyle.length > 2)
      return this.cssstyle;
    return "100%";
  }

  showEmpty(): boolean {
    if (this.empty != undefined && this.empty == "true") {
      return !this.shouldShowErrors();
    }
    return false;
  }

  shouldShowErrors(): boolean {
    this.getControl();
    return this.control &&
      this.control.errors && this.myForm.submitted;
      //(this.control.dirty || this.control.touched || this.myForm.submitted);
  }

  listOfErrors(): string[] {
    this.getControl();
    return Object.keys(this.control.errors)
      .map(field => this.getMessage(field, this.control.errors[field]));
  }

  private getMessage(type: string, params: any) {
    if (this.errMessage != undefined && this.errMessage.length > 3)
      return this.errMessage;
    return ShowErrorsComponent.errorMessages[type](params);
  }

}
