import { Component, OnInit, Input, Inject, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ProviderImageComponent } from "../provider-image/provider-image.component"
import { UserService } from '../../services/user.service';
import { NgDatepickerComponent } from "ng2-datepicker"
import { DatepickerOptions } from "ng2-datepicker"
import * as enLocale from 'date-fns/locale/en';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConditionallyValidateService } from 'ng-conditionally-validate';
import { Observable } from 'rxjs/Rx';
import { ConfirmComponent } from '../confirm/confirm.component';
import { DialogService } from "ngx-bootstrap-modal";
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts'
import * as html2canvas from 'html2canvas';
import { EmailDirective } from '../../services/email.directive';
import $ from 'jquery/dist/jquery';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['../../../assets/css/index.css']
})
export class HomeComponent implements OnInit {
  public bseUrl: string = "";
  public surveyAlreadyExists: boolean = false;
  public verifyServiceAddress: boolean = true;
  public verifyServiceAddressNo: boolean = false;
  public profileId: number = null;
  public provId: number = null;
  public notPrinting: boolean = true;

  form22: FormGroup;
  public e : CustMember = {
    otherWaterUsingEquipmentDesc : "",
    otherPropertyTypeValue: "",
    contactEmailAddress: "",
    otherConnectedPlant: "",
    connectedPlants: new Array(),
    arrConnectedPlants: new Array(),
    waterReceivers: new Array(),
    arrWaterReceivers: new Array(),
    surveyOwnerFullName: "",
    surveyDate: new Date(),
    surveyOwnerPhone: "",
    Customer: null,
    residentialPropType: false,
    commercialPropType: false,
    otherPropType: false,
    otherConnectPlant: false,
    otherEquipmentYes: false,
    otherEquipmentNo: false,
    otherEquipmentUnsure: false,
    contactByEmailYes: false,
    contactByEmailNo: false,
    backflowPrevDeviceUnsure: false,
    backflowPrevDeviceNo: false,
    backflowPrevDeviceYes: false,
  };
  calOptions: DatepickerOptions = {
    locale: enLocale,
    minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now()),
  };
  public otherWaterUsingEquipmentDesc: string = "";
  public otherPropertyTypeValue: string = "";
  public contactEmailAddress: string = "";
  public otherConnectedPlant: string = "";
  public otherConnectPlant: boolean = false;

  public connectedPlants: string[] = new Array();
  public arrConnectedPlants: ConnectedPlant[] = new Array();

  public waterReceivers: string[] = new Array();
  public arrWaterReceivers: WaterReceiver[] = new Array();

  public surveyOwnerFullName: string = "";
  public surveyDate: Date;
  public surveyOwnerPhone: string = "";

  public Customer: Customer = null;
  public residentialPropType: boolean = false;
  public commercialPropType: boolean = false;
  public otherPropType: boolean = false;

  public otherEquipmentYes: boolean = false;
  public otherEquipmentNo: boolean = false;
  public otherEquipmentUnsure: boolean = false;

  public contactByEmailYes: boolean = false;
  public contactByEmailNo: boolean = false;
  public backflowPrevDeviceUnsure: boolean = false;
  public backflowPrevDeviceNo: boolean = false;
  public backflowPrevDeviceYes: boolean = false;

  public displayOtherProp$: Observable<boolean>;

  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')',' ',/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private router: Router, public http: Http, @Inject('BASE_URL') baseUrl: string, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef, private user: UserService, private cv: ConditionallyValidateService, private fb: FormBuilder, private dialogService: DialogService, private spinnerService: Ng4LoadingSpinnerService) {
    this.bseUrl = baseUrl;
    this.surveyDate = new Date();
    this.toastr.setRootViewContainerRef(vcr);

    this.form22 = fb.group({
      otherWaterUsingEquipmentDesc:'',
      backflowPrevDeviceNo: [false],
      backflowPrevDeviceYes: [false],
      backflowPrevDeviceUnsure: [false],
      contactByEmailNo: [false],
      contactByEmailYes: [false],
      otherEquipmentUnsure: [false],
      otherEquipmentNo: [false],
      otherEquipmentYes: [false],
      otherPropType: [false],
      otherConnectPlant: [false],
      commercialPropType: [false],
      residentialPropType: [false],
      contactYes: [false],
      contactEmailAddress: [''],
      surveyOwnerFullName: ['', Validators.required],
      surveyOwnerPhone: ['', Validators.required],
      otherConnectedPlant: '',
      otherPropertyTypeValue: '',
      surveyDate: [new Date()]
    })
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    cv.validate(this.form22, 'contactEmailAddress')
      .using(Validators.required, EmailDirective.email2)
      .when('contactByEmailYes')
      .is(true)

    cv.validate(this.form22, 'otherPropertyTypeValue').using(Validators.required).when('otherPropType').is(true)
    cv.validate(this.form22, 'otherConnectedPlant').using(Validators.required).when('otherConnectPlant').is(true)
    cv.validate(this.form22, 'otherWaterUsingEquipmentDesc').using(Validators.required).when('otherEquipmentYes').is(true)      
  }

  printSurveyForm() {
    this.notPrinting = false;
    setTimeout(function () {
      window.print();
    }, 300);
  }

  ngOnInit() {
    (<any>window).homeComponent = this;
    this.Customer = this.route.snapshot.data['customer'];
    this.profileId = this.Customer.customerId;
    for (var i = 0; i <= 28; i++) {
      this.connectedPlants[i] = "";
      this.waterReceivers[i] = "";
    }

    var ccId = this.route.snapshot.paramMap.get("ccId");
    if (ccId != undefined && ccId.length > 2) {
      var toast = this.toastr;
      setTimeout(function () {
        toast.success("Retrieved Survey with CCID# :" + ccId, "Survey Retrieved successfully!", { toastLife: 1000 });
      }, 300);
    }

    var afterPrint = function () {
      (<any>window).homeComponent.notPrinting = true;
    };

    if (window.matchMedia) {
      var mediaQueryList = window.matchMedia('print');
      mediaQueryList.addListener(function (mql) {
        if (!mql.matches) {
          afterPrint();
        }
      });
    }

    window.onafterprint = afterPrint;

  }

  clickOtherConnectPlant(e: any) {
    this.otherConnectPlant = e.target.checked;
    this.e.otherConnectPlant = e.target.checked;
    $('input[name=connectedPlant28]').prop("checked", false);
    this.connectedPlants[28] = "";
  }

  updateVerifyAddress($event: any, val: any) {
    if (val != undefined && val == 'No') {
      this.verifyServiceAddress = false;
      this.verifyServiceAddressNo = true;
    }
    else {
      this.verifyServiceAddress = $event.target.checked;
      this.verifyServiceAddressNo = false;
    }
  }
  updateConnectedPlants($event: any) {
    var nm = $event.target.getAttribute("name");
    if (nm == "connectedPlant28" && $event.target.checked) {
      this.e.otherConnectPlant = false;
      this.e.otherConnectedPlant = "";
      for (var i = 0; i <= 28; i++) {
        this.connectedPlants[i] = "";
        var j = i + 1;
        var ctrl = "connectedPlant" + j;
        if (ctrl != "connectedPlant28")
          $('input[name=' + ctrl + ']').prop("checked", false);
      }
    }
    if ($event.target.checked && nm != "connectedPlant28") {
      $('input[name=connectedPlant28]').prop("checked", false);
      this.connectedPlants[28] = "";
    }
    if ($event.target.checked)
      this.connectedPlants[$event.target.getAttribute('idx')] = $event.target.getAttribute('value');
    else
      this.connectedPlants[$event.target.getAttribute('idx')] = "";
  }

  updateWaterReceivers($event: any) {
    var nm = $event.target.getAttribute("name");
    if (nm == "waterReceiver28" && $event.target.checked) {
      for (var i = 0; i <= 28; i++) {
        this.waterReceivers[i] = "";
        var j = i + 1;
        var ctrl = "waterReceiver" + j;
        if (ctrl != "waterReceiver28")
          $('input[name=' + ctrl + ']').prop("checked", false);
      }
    }
    if ($event.target.checked && nm != "waterReceiver28") {
      $('input[name=waterReceiver28]').prop("checked", false);
      this.waterReceivers[28] = "";
    }
    if ($event.target.checked)
      this.waterReceivers[$event.target.getAttribute('idx')] = $event.target.getAttribute('value');
    else
      this.waterReceivers[$event.target.getAttribute('idx')] = "";
  }

  updateContactByEmail(evt: any, e: any) {
    if (e == "yes") {
      if (evt.target.checked) {
        this.contactByEmailYes = true;
        this.contactByEmailNo = false;
        this.e.contactByEmailYes = true;
        this.e.contactByEmailNo = false;
      }
      else {
        this.contactByEmailYes = false;
        this.contactByEmailNo = false;
        this.e.contactByEmailYes = false;
        this.e.contactByEmailNo = false;
      }
    }
    if (e == "no") {
      if (evt.target.checked) {
        this.contactByEmailNo = true;
        this.contactByEmailYes = false;
        this.e.contactByEmailNo = true;
        this.e.contactByEmailYes = false;
      }
      else {
        this.contactByEmailYes = false;
        this.contactByEmailNo = false;
        this.e.contactByEmailYes = false;
        this.e.contactByEmailNo = false;
      }
    }
  }
  updateBackflowDevice(evt:any, e: any) {
    if (e == "yes") {
      if (evt.target.checked) {
        this.backflowPrevDeviceYes = true;
        this.backflowPrevDeviceNo = false;
        this.backflowPrevDeviceUnsure = false;
        this.e.backflowPrevDeviceYes = true;
        this.e.backflowPrevDeviceNo = false;
        this.e.backflowPrevDeviceUnsure = false;
      }
      else {
        this.backflowPrevDeviceYes = false;
        this.backflowPrevDeviceNo = false;
        this.backflowPrevDeviceUnsure = false;
        this.e.backflowPrevDeviceYes = false;
        this.e.backflowPrevDeviceNo = false;
        this.e.backflowPrevDeviceUnsure = false;
      }
    }
    if (e == "no") {
      if (evt.target.checked) {
        this.backflowPrevDeviceNo = true;
        this.backflowPrevDeviceYes = false;
        this.backflowPrevDeviceUnsure = false;
        this.e.backflowPrevDeviceNo = true;
        this.e.backflowPrevDeviceYes = false;
        this.e.backflowPrevDeviceUnsure = false;
      }
      else {
        this.backflowPrevDeviceYes = false;
        this.backflowPrevDeviceNo = false;
        this.backflowPrevDeviceUnsure = false;
        this.e.backflowPrevDeviceYes = false;
        this.e.backflowPrevDeviceNo = false;
        this.e.backflowPrevDeviceUnsure = false;
      }
    }
    if (e == "unsure") {
      if (evt.target.checked) {
        this.backflowPrevDeviceUnsure = true;
        this.backflowPrevDeviceYes = false;
        this.backflowPrevDeviceNo = false;
        this.e.backflowPrevDeviceUnsure = true;
        this.e.backflowPrevDeviceYes = false;
        this.e.backflowPrevDeviceNo = false;
      }
      else {
        this.backflowPrevDeviceYes = false;
        this.backflowPrevDeviceNo = false;
        this.backflowPrevDeviceUnsure = false;
        this.e.backflowPrevDeviceYes = false;
        this.e.backflowPrevDeviceNo = false;
        this.e.backflowPrevDeviceUnsure = false;
      }
    }
  }

  updateOtherEquipment(evt: any, e: any) {
    if (e == "yes") {
      if (evt.target.checked) {
        this.otherEquipmentYes = true;
        this.otherEquipmentNo = false;
        this.otherEquipmentUnsure = false;
        this.e.otherEquipmentYes = true;
        this.e.otherEquipmentNo = false;
        this.e.otherEquipmentUnsure = false;
      }
      else {
        this.otherEquipmentYes = false;
        this.otherEquipmentNo = false;
        this.otherEquipmentUnsure = false;
        this.e.otherEquipmentYes = false;
        this.e.otherEquipmentNo = false;
        this.e.otherEquipmentUnsure = false;
      }
    }
    if (e == "no") {
      if (evt.target.checked) {
        this.otherEquipmentNo = true;
        this.otherEquipmentYes = false;
        this.otherEquipmentUnsure = false;
        this.e.otherEquipmentNo = true;
        this.e.otherEquipmentYes = false;
        this.e.otherEquipmentUnsure = false;
      }
      else {
        this.otherEquipmentYes = false;
        this.otherEquipmentNo = false;
        this.otherEquipmentUnsure = false;
        this.e.otherEquipmentYes = false;
        this.e.otherEquipmentNo = false;
        this.e.otherEquipmentUnsure = false;
      }
    }
    if (e == "unsure") {
      if (evt.target.checked) {
        this.otherEquipmentUnsure = true;
        this.otherEquipmentYes = false;
        this.otherEquipmentNo = false;
        this.e.otherEquipmentUnsure = true;
        this.e.otherEquipmentYes = false;
        this.e.otherEquipmentNo = false;
      }
      else {
        this.otherEquipmentYes = false;
        this.otherEquipmentNo = false;
        this.otherEquipmentUnsure = false;
        this.e.otherEquipmentYes = false;
        this.e.otherEquipmentNo = false;
        this.e.otherEquipmentUnsure = false;
      }
    }
  }

  updatePropertyType(evt: any, e: any) {
    if (e == "residential") {
      if (evt.target.checked) {
        this.otherPropertyTypeValue = "";
        this.e.otherPropertyTypeValue = "";
        this.residentialPropType = true;
        this.commercialPropType = false;
        this.otherPropType = false;
        this.e.residentialPropType = true;
        this.e.commercialPropType = false;
        this.e.otherPropType = false;
      }
      else {
        this.residentialPropType = false;
        this.commercialPropType = false;
        this.otherPropType = false;
        this.e.residentialPropType = false;
        this.e.commercialPropType = false;
        this.e.otherPropType = false;
      }
    }
    if (e == "commercial") {
      if (evt.target.checked) {
        this.otherPropertyTypeValue = "";
        this.e.otherPropertyTypeValue = "";
        this.commercialPropType = true;
        this.residentialPropType = false;
        this.otherPropType = false;
        this.e.commercialPropType = true;
        this.e.residentialPropType = false;
        this.e.otherPropType = false;
      }
      else {
        this.residentialPropType = false;
        this.commercialPropType = false;
        this.otherPropType = false;
        this.e.residentialPropType = false;
        this.e.commercialPropType = false;
        this.e.otherPropType = false;
      }
    }
    if (e == "other") {
      if (evt.target.checked) {
        this.otherPropType = true;
        this.commercialPropType = false;
        this.residentialPropType = false;
        this.e.otherPropType = true;
        this.e.commercialPropType = false;
        this.e.residentialPropType = false;
      }
      else {
        this.residentialPropType = false;
        this.commercialPropType = false;
        this.otherPropType = false;
        this.e.residentialPropType = false;
        this.e.commercialPropType = false;
        this.e.otherPropType = false;
      }
    }
  }

  hasWaterReceivers(): boolean {
    var arrWaterReceivers1 = new Array();

    for (var i = 0; i <= 28; i++) {
      var val = this.waterReceivers[i];
      if (val != undefined && val.length > 1) {
        var p1: WaterReceiver = {
          supplyReceiver: val
        };
        arrWaterReceivers1.push(p1);
      }
    }

    return arrWaterReceivers1.length > 0;
  }

  hasConnectedPlants(): boolean {
    var arrConnectedPlants1 = new Array();

    for (var i = 0; i <= 28; i++) {
      var val = this.connectedPlants[i];
      if (val != undefined && val.length > 1) {
        var p: ConnectedPlant = {
          connectedPlantName: val
        };
        arrConnectedPlants1.push(p);
      }
    }
    if (this.e.otherConnectedPlant != undefined && this.e.otherConnectedPlant.length > 1) {
      var p: ConnectedPlant = {
        connectedPlantName: this.e.otherConnectedPlant
      };
      arrConnectedPlants1.push(p);
    }
    return arrConnectedPlants1.length > 0;
  }

  onPreSubmit(e: any) {
    this.showConfirm("Are you sure you want to submit?", "Would you like to print before submit? If yes then click Cancel and then click the Print button.", e);
  }


  showConfirm(title1, msg, e1) {
    let disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: title1,
      message: msg,
      mainbtn: "Submit Anyway",
    }).subscribe((isConfirmed) => {
      //We get dialog result
      if (isConfirmed) {
        this.notPrinting = false;
        var that = this;

        if (true) {
          setTimeout(function () {
            var opt: Html2Canvas.Html2CanvasOptions = {
              width: 1900,
              height: 3000
            }
            html2canvas(document.getElementsByTagName('body')[0], opt).then(function (canvas) {
              var img = canvas.toDataURL("image/jpeg");
              var dd = {
                content: [{
                  image: img,
                  width: 500,
                }]
              }
              pdfMake.createPdf(dd).getBase64(function (buffer) {
                that.spinnerService.show();
                that.onSubmit(e1, buffer);
                that.notPrinting = true;
              });
              that.notPrinting = true;
            });

          }, 100);
        }
        else {
          //that.onSubmit(e1, null);
        }
        
      }
    });
    //We can close dialog calling disposable.unsubscribe();
    //If dialog was not closed manually close it by timeout
    setTimeout(() => {
      disposable.unsubscribe();
    }, 10000);
  }

  onSubmit(e: any, buffer: any) {
    var propType: string = "residential";
    if (this.e.residentialPropType)
      propType = "residential";
    else if (this.e.commercialPropType)
      propType = "commercial";
    else if (this.e.otherPropType)
      propType = "other";

    var otherEquipment = "yes";
    if (this.e.otherEquipmentYes)
      otherEquipment = "yes";
    else if (this.e.otherEquipmentNo)
      otherEquipment = "no";
    else if (this.e.otherEquipmentUnsure)
      otherEquipment = "unsure";

    var backflow = "yes";
    if (this.e.backflowPrevDeviceYes)
      backflow = "yes";
    else if (this.e.backflowPrevDeviceNo)
      backflow = "no";
    else if (this.e.backflowPrevDeviceUnsure)
      backflow = "unsure";

    this.arrConnectedPlants = new Array();

    for (var i = 0; i <= 28; i++) {
      var val = this.connectedPlants[i];
      if (val != undefined && val.length > 1) {
        var p: ConnectedPlant = {
          connectedPlantName: val
        };
        this.arrConnectedPlants.push(p);
      }
    }
    if (this.e.otherConnectedPlant != undefined && this.e.otherConnectedPlant.length > 1) {
      var p: ConnectedPlant = {
        connectedPlantName: this.e.otherConnectedPlant
      };
      this.arrConnectedPlants.push(p);
    }

    this.arrWaterReceivers = new Array();

    for (var i = 0; i <= 28; i++) {
      var val = this.waterReceivers[i];
      if (val != undefined && val.length > 1) {
        var p1: WaterReceiver = {
          supplyReceiver: val
        };
        this.arrWaterReceivers.push(p1);
      }
    }

    var model: CustSurvey = {
      customerId: this.Customer.customerId,
      customer: this.Customer,
      ccId: this.Customer.ccId,
      serviceClass: this.Customer.serviceClass,
      diameter: this.Customer.diameter,
      propertyType: propType,
      propertyTypeValue: this.e.otherPropertyTypeValue,
      otherWaterUsingEquipment: otherEquipment,
      otherWaterUsingEquipmentDesc: this.e.otherWaterUsingEquipmentDesc,
      backflowPreventionDevice: backflow,
      surveyDate: this.e.surveyDate,
      surveyOwnerFullName: this.e.surveyOwnerFullName,
      surveyOwnerPhone: this.e.surveyOwnerPhone,
      contactEmailAddress: this.e.contactEmailAddress,
      connectedPlants: this.arrConnectedPlants,
      waterReceivers: this.arrWaterReceivers,
      attachment: buffer
    }

    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');

    this.http.post(this.bseUrl + 'api/Survey/addSurvey', JSON.stringify(model),
      { headers: headers }).subscribe(
      response => {
        var r = response.json();
        this.spinnerService.hide();
        this.router.navigate(['cust']).then(x => {
          this.user.resetSelectedCustomer();
          this.toastr.success("Customer survey was submitted successfully with CCID :" + this.Customer.ccId, "Survey created successfully");
        });
        
      }, error => {
        this.spinnerService.hide();
        if (error.status == 409) {
          this.router.navigate(['cust']).then(x => {
            this.user.resetSelectedCustomer();
            this.toastr.error('Survey for the Meter already exists!', 'Survey Already Exists!');
          });
          
          //window.scrollTo(0, 100);
        }
      }
      );

  }
}

export interface Customer {
  customerId: number;
  accountId: number;
  waterMeterNumber: number;
  ccId: string;
  serviceClass: string;
  diameter: number;
}

export interface CustSurvey {
  customerId: number;
  customer: Customer;
  ccId: string;
  serviceClass: string;
  diameter: number;
  propertyType: string;
  propertyTypeValue: string;
  backflowPreventionDevice: string;
  otherWaterUsingEquipment: string;
  otherWaterUsingEquipmentDesc: string;
  surveyDate: Date;
  surveyOwnerPhone: string;
  surveyOwnerFullName: string;
  contactEmailAddress: string;
  connectedPlants: ConnectedPlant[];
  waterReceivers: WaterReceiver[];
  attachment: string;
}

export interface ConnectedPlant {
  connectedPlantName: string;
}

export interface WaterReceiver {
  supplyReceiver: string;
}

export interface CustMember {
  otherWaterUsingEquipmentDesc: string;
  otherPropertyTypeValue: string;
  contactEmailAddress: string;
  otherConnectedPlant: string;
  connectedPlants: string[];
  arrConnectedPlants: ConnectedPlant[];
  waterReceivers: string[];
  arrWaterReceivers: WaterReceiver[];

  surveyOwnerFullName: string;
  surveyDate: Date;
  surveyOwnerPhone: string;

  Customer: Customer;
  residentialPropType: boolean;
  commercialPropType: boolean;
  otherPropType: boolean;
  otherConnectPlant: boolean;
  otherEquipmentYes: boolean;
  otherEquipmentNo: boolean;
  otherEquipmentUnsure: boolean;

  contactByEmailYes: boolean;
  contactByEmailNo: boolean;
  backflowPrevDeviceUnsure: boolean;
  backflowPrevDeviceNo: boolean;
  backflowPrevDeviceYes: boolean;
}
