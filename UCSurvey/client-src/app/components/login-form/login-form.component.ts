import { Component, OnInit, Input, Inject, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserService } from '../../services/user.service';
import { AsyncLocalStorage } from 'angular-async-local-storage';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css', '../../../assets/css/index.css', '../../../assets/css/login.css']
})
export class LoginFormComponent implements OnInit {

  public email: string = '';
  public password: string = '';
  public rememberMe: boolean = false;
  public bseUrl: string = "";
  public invalidLogin: boolean = false;
  constructor(private router: Router, private user: UserService, protected localStorage: AsyncLocalStorage, public http: Http, @Inject('BASE_URL') baseUrl: string, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.bseUrl = baseUrl;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    var that = this;
    setTimeout(function () {
      var val = that.user.getUserLoggedOut();
      val.subscribe(x => {
        if (x) {
          that.toastr.success("You have been successfully logged out", "Log out successful!");
          that.user.resetUserLoggedOut();
        }
      })
    }, 300)

  }

  isEmailValid() {
    var EMAIL_REGEXP = /^[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-z]{2,4})$/;
    if (this.email != undefined && this.email.length > 1) {
      var isMatchRegex = EMAIL_REGEXP.test(this.email);
      if (isMatchRegex)
        this.router.navigate(['forgotPassword', { e: this.email }]);
    }
  }

  performLogin() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    this.http.post(this.bseUrl + 'api/Login/', JSON.stringify({ Email: this.email, Password: this.password }),
      { headers: headers }).subscribe(
      r => {
        if (r.status == 200) {
          this.invalidLogin = false;
          this.localStorage.setItem('email', this.email).subscribe(() => { });
          this.localStorage.setItem('password', this.password).subscribe(() => { });
          var params = r.text().replace(/\"/g, "").split(";");
          this.localStorage.setItem('userType', params[0]).subscribe(() => { });
          this.localStorage.setItem('userName', params[1]).subscribe(() => { });
          this.user.setUserLoggedIn().subscribe(x => {
            this.router.navigate(['cust']);
          })
        }
        else {
          this.invalidLogin = true;
        }
      }, error => {
      }
      );
  }

loginUser(e:any) {
    e.preventDefault();

  this.performLogin();
}
}

export interface CustUser {
  Id: number;
  email: string;
  password: string;
} 
