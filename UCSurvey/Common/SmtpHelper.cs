using Microsoft.Extensions.Options;
using System;
using System.Configuration;
using System.Net.Mail;
using System.Reflection;

namespace UCSurvey.Common
{
  public class SmtpClientEx : SmtpClient
  {
    private static readonly FieldInfo _localHostName = GetLocalHostNameField();
    public IOptions<SmtpSettings> SmtpSettings { get; set; }
    /// <summary>
    /// Initializes a new instance of the <see cref="SmtpClientEx"/> class
    /// that sends e-mail by using the specified SMTP server and port.
    /// </summary>
    /// <param name="host">
    /// A <see cref="String"/> that contains the name or 
    /// IP address of the host used for SMTP transactions.
    /// </param>
    /// <param name="port">
    /// An <see cref="Int32"/> greater than zero that 
    /// contains the port to be used on host.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="port"/> cannot be less than zero.
    /// </exception>
    public SmtpClientEx(string host, int port)
            : base(host, port)
    {
      Initialize();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="SmtpClientEx"/> class
    /// that sends e-mail by using the specified SMTP server.
    /// </summary>
    /// <param name="host">
    /// A <see cref="String"/> that contains the name or 
    /// IP address of the host used for SMTP transactions.
    /// </param>
    public SmtpClientEx(string host)
        : base(host)
    {
      Initialize();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="SmtpClientEx"/> class
    /// by using configuration file settings.
    /// </summary>
    public SmtpClientEx()
    {
      Initialize();
    }

    /// <summary>
    /// Gets or sets the local host name used in SMTP transactions.
    /// </summary>
    /// <value>
    /// The local host name used in SMTP transactions.
    /// This should be the FQDN of the local machine.
    /// </value>
    /// <exception cref="ArgumentNullException">
    /// The property is set to a value which is
    /// <see langword="null"/> or <see cref="String.Empty"/>.
    /// </exception>
    public string LocalHostName
    {
      get
      {
        if (null == _localHostName) return null;
        return (string)_localHostName.GetValue(this);
      }
      set
      {
        if (string.IsNullOrEmpty(value))
        {
          throw new ArgumentNullException("value");
        }
        if (null != _localHostName)
        {
          _localHostName.SetValue(this, value);
        }
      }
    }

    /// <summary>
    /// Returns the price "localHostName" field.
    /// </summary>
    /// <returns>
    /// The <see cref="FieldInfo"/> for the private
    /// "localHostName" field.
    /// </returns>
    private static FieldInfo GetLocalHostNameField()
    {
      BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
      return typeof(SmtpClient).GetField("localHostName", flags);
    }

    /// <summary>
    /// Initializes the local host name to 
    /// the FQDN of the local machine.
    /// </summary>
    private void Initialize()
    {
      if(SmtpSettings != null)
      if (SmtpSettings.Value != null && !string.IsNullOrWhiteSpace(SmtpSettings.Value.NotificationEHLOName))
      {
        //IPGlobalProperties ip = IPGlobalProperties.GetIPGlobalProperties();
        //if (!string.IsNullOrEmpty(ip.HostName) && !string.IsNullOrEmpty(ip.DomainName))
        //{
        //    this.LocalHostName = ip.HostName + "." + ip.DomainName;
        //}
        this.LocalHostName = SmtpSettings.Value.NotificationEHLOName;
      }
    }
  }
}
