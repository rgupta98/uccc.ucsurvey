using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UCSurvey.Common
{
  public class SmtpSettings
  {
    public string NotificationEHLOName { get; set; }
    public bool EnableNotification { get; set; }
    public string SmtpHost { get; set; }
    public string SmtpPort { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public bool EnableSsl { get; set; }
  }
}
