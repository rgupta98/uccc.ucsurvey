using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UCSurvey.Common
{
    public class EmailValidator
    {
        private const string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                 @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                 @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        private String email;

        public EmailValidator(string email)
        {
            this.email = email;
        }

        public EmailValidator()
        {
        }

        public void Validate()
        {
            if (string.IsNullOrEmpty(email))
                throw new ValidationException("Email should not be null.");

            Regex re = new Regex(strRegex);
            string[] emails = email.Split(";,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string eMail in emails)
            {
                if (!re.IsMatch(eMail.Trim()))
                    throw new ValidationException("Email Format is not valid.");
            }
        }

        public bool ValidateSingleEmail(string emailId)
        {
            var re = new Regex(strRegex);
            return re.IsMatch(emailId);
        }
    }

    public class ValidationException : ApplicationException
    {
        public object[] Args { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>

        public ValidationException(String message, params object[] args)
            : base(message)
        {
            Args = args;
        }
    }
}
