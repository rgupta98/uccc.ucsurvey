using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace UCSurvey.Data
{
  public class CustSurvey
  {
    [Key]
    public virtual int CustSurveyId { get; set; }
    public virtual int CustomerId { get; set; }
    public virtual Customer Customer { get; set; }
    public string ServiceClass { get; set; }
    public long Diameter { get; set; }
    public string PropertyType { get; set; }
    public string PropertyTypeValue { get; set; }
    public string BackflowPreventionDevice { get; set; }
    public string OtherWaterUsingEquipment { get; set; }
    public string OtherWaterUsingEquipmentDesc { get; set; }
    public string SurveyOwnerFullName { get; set; }
    public string SurveyOwnerPhone { get; set; }
    public DateTime SurveyDate { get; set; }
    public string ContactEmailAddress { get; set; }
    public virtual IList<ConnectedPlant> ConnectedPlants { get; set; }
    public virtual IList<WaterReceiver> WaterReceivers { get; set; }
    [NotMapped]
    public virtual Boolean ContactByEmailYes { get; set; }
    [NotMapped]
    public virtual string Attachment { get; set; }
  }
}
