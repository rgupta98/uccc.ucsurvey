using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UCSurvey.Data
{
  public class Customer
  {
    [Key]
    public virtual int CustomerId { get; set; }
    public string CCId { get; set; }
    public virtual string CustomerName { get; set; }
    public virtual int WaterProviderId { get; set; }
    public virtual long AccountId { get; set; }
    public virtual long WaterMeterNumber { get; set; }
    public virtual long Diameter { get; set; }
    public virtual string ServiceClass { get; set; }
    public virtual string ConnectionStatus { get; set; }
    public virtual WaterProvider WaterProvider { get; set; }
    [JsonIgnore]
    public virtual IList<CustSurvey> CustSurveys { get; set; }
    public virtual string ServiceAddrStreetNumber { get; set; }
    public virtual string ServiceAddrStreetNumberSuffix { get; set; }
    public virtual string ServiceAddrStreetDirPrefix { get; set; }
    public virtual string ServiceAddrStreetName { get; set; }
    public virtual string ServiceAddrStreetType { get; set; }
    public virtual string ServiceAddrStreetDirSuffix { get; set; }
    public virtual string ServiceAddrCityName { get; set; }
    public virtual string ServiceAddrState { get; set; }
    public virtual string ServiceAddrZipCode { get; set; }

    public virtual string BillingAddrStreetNumber { get; set; }
    public virtual string BillingAddrStreetNumberSuffix { get; set; }
    public virtual string BillingAddrStreetDirPrefix { get; set; }
    public virtual string BillingAddrStreetName { get; set; }
    public virtual string BillingAddrStreetType { get; set; }
    public virtual string BillingAddrStreetDirSuffix { get; set; }
    public virtual string BillingAddrAptDesc { get; set; }
    public virtual string BillingAddrAptNumber { get; set; }
    public virtual string BillingAddrCityName { get; set; }
    public virtual string BillingAddrState { get; set; }
    public virtual string BillingAddrZipCode { get; set; }

    public virtual string OwnerAddrStreetNumber { get; set; }
    public virtual string OwnerAddrStreetNumberSuffix { get; set; }
    public virtual string OwnerAddrStreetDirPrefix { get; set; }
    public virtual string OwnerAddrStreetName { get; set; }
    public virtual string OwnerAddrStreetType { get; set; }
    public virtual string OwnerAddrStreetDirSuffix { get; set; }
    public virtual string OwnerAddrAptDesc { get; set; }
    public virtual string OwnerAddrAptNumber { get; set; }
    public virtual string OwnerAddrCityName { get; set; }
    public virtual string OwnerAddrState { get; set; }
    public virtual string OwnerAddrZipCode { get; set; }
    public virtual string OwnerPlaceOfWork { get; set; }
    public virtual string OwnerFirstName { get; set; }
    public virtual string OwnerLastName { get; set; }
    public virtual DateTime? DueDate { get; set; }
    [NotMapped]
    public virtual string DueDateString { get; set; }
    [NotMapped]
    public virtual string ServiceAddress { get; set; }
    [NotMapped]
    public virtual string BillingAddress { get; set; }

    public virtual string GetServiceAddress()
    {
      var address = $"{ServiceAddrStreetNumber} {ServiceAddrStreetNumberSuffix} {ServiceAddrStreetDirPrefix} {ServiceAddrStreetName} {ServiceAddrStreetType} {ServiceAddrStreetDirSuffix} {ServiceAddrCityName} {ServiceAddrState} {ServiceAddrZipCode}";
      address = Regex.Replace(address, @"\s+", " ");
      return address;
    }

    public virtual string GetBillingAddress()
    {
      var address = $"{BillingAddrStreetNumber} {BillingAddrStreetNumberSuffix} {BillingAddrStreetDirPrefix} {BillingAddrStreetName} {BillingAddrStreetType} {BillingAddrStreetDirSuffix} {BillingAddrAptDesc} {BillingAddrAptNumber} {BillingAddrCityName} {BillingAddrState} {BillingAddrZipCode}";
      address = Regex.Replace(address, @"\s+", " ");
      return address;
    }

    public virtual void Update(Customer cust)
    {
      CustomerName = cust.CustomerName;
      AccountId = cust.AccountId;
      WaterMeterNumber = cust.WaterMeterNumber;
      Diameter = cust.Diameter;
      ServiceClass = cust.ServiceClass;
      ConnectionStatus = cust.ConnectionStatus;
      ServiceAddrZipCode = cust.ServiceAddrZipCode;
      ServiceAddrStreetType = cust.ServiceAddrStreetType;
      ServiceAddrStreetNumberSuffix = cust.ServiceAddrStreetNumberSuffix;
      ServiceAddrStreetNumber = cust.ServiceAddrStreetNumber;
      ServiceAddrStreetName = cust.ServiceAddrStreetName;
      ServiceAddrStreetDirSuffix = cust.ServiceAddrStreetDirSuffix;
      ServiceAddrStreetDirPrefix = cust.ServiceAddrStreetDirPrefix;
      ServiceAddrState = cust.ServiceAddrState;
      ServiceAddrCityName = cust.ServiceAddrCityName;

      BillingAddrZipCode = cust.BillingAddrZipCode;
      BillingAddrStreetType = cust.BillingAddrStreetType;
      BillingAddrStreetNumberSuffix = cust.BillingAddrStreetNumberSuffix;
      BillingAddrStreetNumber = cust.BillingAddrStreetNumber;
      BillingAddrStreetName = cust.BillingAddrStreetName;
      BillingAddrStreetDirSuffix = cust.BillingAddrStreetDirSuffix;
      BillingAddrStreetDirPrefix = cust.BillingAddrStreetDirPrefix;
      BillingAddrState = cust.BillingAddrState;
      BillingAddrCityName = cust.BillingAddrCityName;
      BillingAddrAptDesc = cust.BillingAddrAptDesc;
      BillingAddrAptNumber = cust.BillingAddrAptNumber;

      OwnerAddrZipCode = cust.OwnerAddrZipCode;
      OwnerAddrStreetType = cust.OwnerAddrStreetType;
      OwnerAddrStreetNumberSuffix = cust.OwnerAddrStreetNumberSuffix;
      OwnerAddrStreetNumber = cust.OwnerAddrStreetNumber;
      OwnerAddrStreetName = cust.OwnerAddrStreetName;
      OwnerAddrStreetDirSuffix = cust.OwnerAddrStreetDirSuffix;
      OwnerAddrStreetDirPrefix = cust.OwnerAddrStreetDirPrefix;
      OwnerAddrState = cust.OwnerAddrState;
      OwnerAddrCityName = cust.OwnerAddrCityName;
      OwnerAddrAptDesc = cust.OwnerAddrAptDesc;
      OwnerAddrAptNumber = cust.OwnerAddrAptNumber;
      OwnerPlaceOfWork = cust.OwnerPlaceOfWork;
      OwnerFirstName = cust.OwnerFirstName;
      OwnerLastName = cust.OwnerLastName;
      var dateTime = new DateTime();
      if(DateTime.TryParse(cust.DueDateString, out dateTime))
      {
        DueDate = dateTime;
      }
    }

  }
}
