using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UCSurvey.Data
{
  public class ConnectedPlant
  {
    [Key]
    public virtual int ConnectedPlantId { get; set; }
    public virtual int CustSurveyId { get; set; }
    public virtual CustSurvey CustSurvey { get; set; }
    public virtual string ConnectedPlantName { get; set; }

  }
}
