using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UCSurvey.Data
{
  public class CustUser
  {
    [Key]
    public virtual int CustUserId { get; set; }
    public virtual string Phone { get; set; }
    public virtual string Email { get; set; }
    public virtual string FirstName { get; set; }
    public virtual string LastName { get; set; }
    public virtual string UserType { get; set; }
    public virtual string Password { get; set; }
  }

  public class ChangeUserPass
  {
    public string UserEmail { get; set; }
    public string UserPhone { get; set; }
    public string UserPassword { get; set; }
    public string ConfirmPassword { get; set; }
  }

}
