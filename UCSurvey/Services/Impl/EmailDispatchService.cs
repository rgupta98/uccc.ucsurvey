using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using UCSurvey.Common;

namespace UCSurvey.Services.Impl
{
  public class EmailDispatchService : IEmailDispatchService
  {
    private IOptions<SmtpSettings> _smtpSettings;
    public EmailDispatchService(IOptions<SmtpSettings> smtpSettings)
    {
      _smtpSettings = smtpSettings;
    }
    public async Task<bool> DispatchNotification(string subject, string from, string to, string cc, string bcc, string body, List<string> attachments, bool isHTML, String replyTo = "")
    {
      var message = new MailMessage();
      foreach (var objString in attachments)
      {
        if (!string.IsNullOrWhiteSpace(objString))
        {
          var attachment = new Attachment(objString);
          message.Attachments.Add(attachment);
        }
      }
      return await CreateMessage(subject, from, to, cc, bcc, body, isHTML, message, replyTo);
    }


    #region private Helper Methods

    public async Task<bool> CreateMessage(string subject, string from, string to, string cc, string bcc, string body, bool isHTML, MailMessage message, string replyTo = "")
    {
      bool isNotificationEnabled = _smtpSettings.Value.EnableNotification;

      string subjectPrefix = string.Empty;

      if (!isNotificationEnabled) return false;

      message.Subject = subjectPrefix + subject;
      message.Body = body;
      message.IsBodyHtml = isHTML;

      //specify the priority of the mail message
      if (!string.IsNullOrWhiteSpace(replyTo))
        message.ReplyToList.Add(replyTo);

      AddRecipients(to, message.To);
      if (message.To.Count == 0) return false;
      if (!string.IsNullOrWhiteSpace(cc)) AddRecipients(cc, message.CC);
      if (!string.IsNullOrWhiteSpace(bcc)) AddRecipients(bcc, message.Bcc);

      CheckEmailValidity(@from);
      if (!String.IsNullOrEmpty(@from))
      {
        string fromId = string.Empty;
        string fromDisplayName = string.Empty;

        string[] fromMails = @from.Split(',', ';');
        if (fromMails.Length > 0)
          fromId = fromMails[0];

        if (fromMails.Length > 1)
          fromDisplayName = fromMails[1];

        message.From = new MailAddress(fromId, fromDisplayName);
      }

      return await SendMessage(message);
    }

    private async Task<bool> SendMessage(MailMessage message)
    {
      var result = false;
      if (_smtpSettings.Value.SmtpHost == null)
        throw new Exception("Smtp Server Not configured");

      var smtpClient = new SmtpClientEx { Host = _smtpSettings.Value.SmtpHost, SmtpSettings = _smtpSettings };

      if (_smtpSettings.Value.SmtpPort != null)
        smtpClient.Port = Convert.ToInt32(_smtpSettings.Value.SmtpPort);

      if (_smtpSettings.Value.UserName != null)
      {
        smtpClient.Credentials = new NetworkCredential(_smtpSettings.Value.UserName, _smtpSettings.Value.Password);
        smtpClient.EnableSsl = _smtpSettings.Value.EnableSsl;
      }
      try
      {
        smtpClient.SendCompleted += (s, e) =>
        {
          if (e.Error != null)
          {
            result = false;
          }
          else
            result = true;
          smtpClient.Dispose();
          message.Dispose();
        };
        await smtpClient.SendMailAsync(message);
        return result;
        //smtpClient.SendAsync(message, null);
      }
      catch (Exception)
      {
      }
      return false;
    }

    private static void AddRecipients(string emailAddresses, MailAddressCollection recipients)
    {
      CheckEmailValidity(emailAddresses);
      if (!String.IsNullOrEmpty(emailAddresses))
      {
        string[] eMails = emailAddresses.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
        var distinctEmails = new HashSet<string>();
        foreach (var eMail in eMails)
        {
          if (distinctEmails.Add(eMail.Trim()))
            recipients.Add(new MailAddress(eMail.Trim()));
        }
      }
    }

    private static void CheckEmailValidity(string email)
    {
      var emailValidator = new EmailValidator(email);
      emailValidator.Validate();
    }

    #endregion
  }
}
