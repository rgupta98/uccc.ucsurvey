using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace UCSurvey.Services
{
  public interface IEmailDispatchService
  {
    Task<bool> DispatchNotification(string subject, string from, string to, string cc, string bcc, string body, List<string> attachments, bool isHTML, String replyTo = "");
    Task<bool> CreateMessage(string subject, string from, string to, string cc, string bcc, string body, bool isHTML, MailMessage message, string replyTo = "");
  }
}
