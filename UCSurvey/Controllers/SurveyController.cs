using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UCSurvey.Data;
using UCSurvey.Services;
using UCSurvey.Utils;

namespace UCSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/Survey")]
  public class SurveyController : Controller
  {
    private readonly UCCCContext _context;
    private readonly IEmailDispatchService _emailSvc;
    public SurveyController(UCCCContext context, IEmailDispatchService svc)
    {
      _context = context;
      _emailSvc = svc;
    }

    [HttpPost()]
    [Route("addSurvey")] //
    public async Task<IActionResult> Post([FromBody]CustSurvey survey)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      //survey.CCId = GenerateCCId();
      var ccId = survey.Customer.CCId;
      //if (SurveyExists(ccId))
      //{
      //  return new StatusCodeResult(StatusCodes.Status409Conflict);
      //}
      survey.Customer = null;
      if(survey.ConnectedPlants != null)
      {
        foreach (var item in survey.ConnectedPlants)
        {
          item.CustSurvey = survey;
        }
      }
      if(survey.WaterReceivers != null)
      {
        foreach (var item in survey.WaterReceivers)
        {
          item.CustSurvey = survey;
        }
      }

      _context.CustSurveys.Add(survey);
      try
      {
        await _context.SaveChangesAsync();
        var user = HttpContext.Session.Get<CustUser>("CurrentUser");
        if (user != null && !string.IsNullOrWhiteSpace(user.Email))
        {
          EmailHelper.Send(ccId, survey.SurveyOwnerFullName, user.Email, _emailSvc, survey.Attachment);
        }
      }
      catch (DbUpdateException)
      {
          throw;
      }

      return CreatedAtAction("GetSurvey", new { id = ccId }, survey);
    }

    private bool SurveyExists(string ccId)
    {
      return _context.CustSurveys.Any(e => e.Customer.CCId == ccId);
    }
    private string GenerateCCId()
    {
      var cust = HttpContext.Session.Get<Customer>("UsersCustomer");
      if (cust != null && cust.WaterProvider != null)
      {
        var otherCustomers = _context.Customers.Where(x => x.AccountId == cust.AccountId).OrderBy(x => x.WaterMeterNumber).ToList();
        int cnt = 1;
        for (int i = 0; i < otherCustomers.Count; i++)
        {
          if(otherCustomers[i].WaterMeterNumber == cust.WaterMeterNumber)
          {
            cnt = i + 1;
            break;
          }
        }
        return $"{cust.WaterProvider.ProviderName}-{cust.AccountId}-{cnt}";
      }
      return string.Empty;
    }

    [HttpGet]
    [Route("searchCCID/{q?}")]
    public List<string> SearchCCID(string q = "")
    {
      var result = new List<string>();
      if (!string.IsNullOrWhiteSpace(q))
      {
        return _context.Customers.Where(x => x.CCId.ToLower().Contains(q.ToLower())).Select(x => x.CCId).ToList();
      }
      return result;
    }

  }
}
