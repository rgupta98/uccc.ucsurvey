using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UCSurvey.Data;
using UCSurvey.Utils;

namespace UCSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/Provider")]
  public class ProviderController : Controller
  {
    private readonly UCCCContext _context;
    public ProviderController(UCCCContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("getImage/{customerId}")] //  
    public string GetImage(int customerId)
    {
      var cust = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CustomerId == customerId);
      if (cust != null)
        return System.Convert.ToBase64String(cust.WaterProvider.Logo);
      return string.Empty;

    }

    [HttpGet]
    [Route("getProvImage/{providerId}")] //  
    public string GetProvImage(int providerId)
    {
      var prov = _context.WaterProviders.FirstOrDefault(x => x.WaterProviderId == providerId);
      if (prov != null)
        return System.Convert.ToBase64String(prov.Logo);
      return string.Empty;
    }

    [HttpGet]
    [Route("getCustomer/{customerId}")] //
    public Customer GetCust(int customerId)
    {
      var customer = HttpContext.Session.Get<Customer>("UsersCustomer");
      if (customer != null)
        return customer;
      else
      {
        var cust = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CustomerId == customerId);
        return cust;
      }
    }

    [HttpGet]
    [Route("getCustomer2/{customerId}")] //
    public Customer GetCust2(int customerId)
    {
      var cust = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CustomerId == customerId);
      return cust;
    }

    [HttpGet]
    [Route("getProviders")] //  
    public List<WaterProv> GetProviders()
    {
      var result = new List<WaterProv>() { new WaterProv() { Key = "Select", Value = 0 } };
      var prov = _context.WaterProviders.OrderBy(x => x.WaterProviderId).ToList();
      foreach (var item in prov)
      {
        var r = new WaterProv()
        {
          Value = item.WaterProviderId,
          Key = item.ProviderName
        };
        result.Add(r);
      }
      return result;
    }

    [HttpGet]
    [Route("searchCustomer/{ccId}")]
    public Customer SearchCust(string ccId)
    {
      if (string.IsNullOrWhiteSpace(ccId))
        return new Customer();
      var cust = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CCId.ToLowerInvariant() == ccId.ToLowerInvariant());
      if (cust == null)
      {
        if(ccId.LastIndexOf('-') == ccId.Length - 2)
        {
          ccId = $"{ccId.Substring(0, ccId.LastIndexOf('-')+1)}0{ccId[ccId.Length - 1]}";
          cust = _context.Customers.Include(x => x.WaterProvider).FirstOrDefault(x => x.CCId.ToLowerInvariant() == ccId.ToLowerInvariant());
          if(cust == null)
            return new Customer();
        }
        else
          return new Customer();
      }

      var survey = _context.CustSurveys.FirstOrDefault(x => x.Customer.CCId == ccId);

      cust.ServiceAddress = cust.GetServiceAddress();
      cust.BillingAddress = cust.GetBillingAddress();

      HttpContext.Session.Set<Customer>("UsersCustomer", cust);
      HttpContext.Session.Set<WaterProvider>("UsersWaterProvider", cust.WaterProvider);

      if (survey != null)
      {
        cust.ServiceAddress = "exists";
        cust.BillingAddress = $"{survey.SurveyOwnerFullName} on {survey.SurveyDate.ToString("MM/dd/yyyy")}";
      }

      return cust;
    }

    [HttpGet]
    [Route("resetCustomer/{ccId}")]
    public Customer ResetCust(string ccId)
    {
      var cust = new Customer();
      if (string.IsNullOrWhiteSpace(ccId))
        return cust;

      HttpContext.Session.Set<Customer>("UsersCustomer", cust);
      HttpContext.Session.Set<WaterProvider>("UsersWaterProvider", cust.WaterProvider);

      return cust;
    }

  }
}
