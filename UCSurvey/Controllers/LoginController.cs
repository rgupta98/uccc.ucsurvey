using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UCSurvey.Data;
using UCSurvey.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;

namespace UCSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class LoginController : Controller
  {

    private readonly UCCCContext _context;

    public LoginController(UCCCContext context)
    {
      _context = context;
    }

    [HttpPost]
    [Route("changePass")] //  
    public async Task<IActionResult> ChangePass([FromBody]ChangeUserPass userDetails)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      var user = _context.CustUsers.FirstOrDefault(x => x.Email.ToLower() == userDetails.UserEmail.ToLower() && x.Phone.ToLower().Replace("-", "").Replace(".", "").Replace(" ", "").Replace("(", "").Replace(")", "") == userDetails.UserPhone.ToLower().Replace("-", "").Replace(".", "").Replace(" ", "").Replace("(", "").Replace(")", ""));
      if(user == null || userDetails.UserPassword == null || userDetails.ConfirmPassword == null || userDetails.UserPassword != userDetails.ConfirmPassword)
      {
        return new StatusCodeResult(StatusCodes.Status203NonAuthoritative);
      }
      var hash = HashPassword.GetHash(userDetails.UserPassword);
      user.Password = hash;
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateException)
      {
        return new StatusCodeResult(StatusCodes.Status203NonAuthoritative);
      }
      return new StatusCodeResult(StatusCodes.Status200OK);
    }

    [HttpGet]
    [Route("getSession")] //  
    public bool SessionExists()
    {
      var user = HttpContext.Session.Get<CustUser>("CurrentUser");
      if (user != null)
        return true;
      return false;
    }

    // GET api/login/5
    [HttpPost()]
    [Route("addUser")] //  
    public async Task<IActionResult> AddUser([FromBody]CustUser user1)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      if(string.IsNullOrWhiteSpace(user1.Email))
        return new StatusCodeResult(StatusCodes.Status203NonAuthoritative);

      var user = _context.CustUsers.FirstOrDefault(x => x.Email.ToLower() == user1.Email.ToLower());
      var hash = HashPassword.GetHash(user1.Password);
      if (user != null)
      {
        return new StatusCodeResult(StatusCodes.Status203NonAuthoritative);
      }
      else
      {
        user1.Password = hash;
        _context.CustUsers.Add(user1);
        try
        {
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateException)
        {
          throw;
        }
        return new StatusCodeResult(StatusCodes.Status200OK);
      }
    }

    // GET api/login/5
    [HttpPost()]
    public IActionResult Post([FromBody]CustUser user1)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      var user = _context.CustUsers.FirstOrDefault(x => x.Email == user1.Email);

      var hash = HashPassword.GetHash(user1.Password);
      if (user != null && user.Password == hash)
      {
        HttpContext.Session.Set<CustUser>("CurrentUser", user);
        return Ok($"{user.UserType};{user.FirstName} {user.LastName}");
      }
      else
      {
        return new StatusCodeResult(StatusCodes.Status203NonAuthoritative);
      }
    }

    [HttpGet]
    [Route("logout")]
    public IActionResult Logout()
    {
      HttpContext.Session.Set<CustUser>("CurrentUser", null);
      HttpContext.Session.Set<Customer>("UsersCustomer", null);
      HttpContext.Session.Set<WaterProvider>("UsersWaterProvider", null);
      return new StatusCodeResult(StatusCodes.Status200OK);
    }
  }
}
