using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UCSurvey.Data;
using UCSurvey.Utils;

namespace UCSurvey.Controllers
{
  [Produces("application/json")]
  [Route("api/Customer")]
  public class CustomerController : Controller
  {
    private readonly UCCCContext _context;
    public CustomerController(UCCCContext context)
    {
      _context = context;
    }

    [HttpGet]
    [Route("searchCust/{q?}")]
    public List<string> SearchCustomer(string q = "")
    {
      var result = new List<string>();
      if (!string.IsNullOrWhiteSpace(q))
      {
        return _context.Customers.Where(x => x.CustomerName.ToLower().Contains(q.ToLower())).Select(x => x.CustomerName).ToList();
      }
      return result;
    }

    [HttpGet]
    [Route("getCustomerName/{name?}")]
    public Customer GetCustomer(string name = "")
    {
      var result = new Customer();
      if (!string.IsNullOrWhiteSpace(name))
      {
        var cust = _context.Customers.FirstOrDefault(x => x.CustomerName.ToLower() == name.ToLower());
        return cust;
      }      
      return result;
    }

    [HttpGet]
    [Route("getCustomerCCID/{name?}")]
    public Customer GetCustomerCCID(string name = "")
    {
      var result = new Customer();
      if (!string.IsNullOrWhiteSpace(name))
      {
        var cust = _context.Customers.FirstOrDefault(x => x.CCId.ToLower() == name.ToLower());
        return cust;
      }
      return result;
    }

    [HttpPost]
    [Route("addCust")]
    public async Task<IActionResult> AddCustomer([FromBody]Customer cust)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      //if (CustomerExists(cust.CustomerName))
      //{
      //  return new StatusCodeResult(StatusCodes.Status409Conflict);
      //}
      if (cust.WaterProviderId <= 0)
        cust.WaterProviderId = 1;
      cust.CCId = GenerateCCId(cust);
      var ccId = cust.CCId;
      _context.Customers.Add(cust);
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateException)
      {
        throw;
      }
      return CreatedAtAction("GetCustomer", new { id = ccId }, cust);
    }

    [HttpPost]
    [Route("editCust")]
    public async Task<IActionResult> EditCustomer([FromBody]Customer cust)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      var cust1 = _context.Customers.FirstOrDefault(x => x.CCId.ToLower() == cust.CCId.ToLower());
      if(cust1 != null)
      {
        cust1.Update(cust);
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateException)
      {
        throw;
      }
      return AcceptedAtAction("GetCustomer", new { id = cust1.CCId }, cust);
    }
    private bool CustomerExists(string name)
    {
      return _context.Customers.Any(e => e.CustomerName.ToLower() == name.ToLower());
    }

    private string GenerateCCId(Customer cust)
    {
      if (cust != null && cust.WaterProviderId > 0)
      {
        var otherCustomers = _context.Customers.Where(x => x.AccountId == cust.AccountId).OrderBy(x => x.WaterMeterNumber).ToList();
        int cnt = 1;
        if (otherCustomers.Count > 0)
          cnt = otherCustomers.Count + 1;
        var provider = _context.WaterProviders.FirstOrDefault(x => x.WaterProviderId == cust.WaterProviderId);
        if(provider != null)
          return $"{provider.ProviderName}-{cust.AccountId}-{cnt}";
      }
      return string.Empty;
    }
  }
}
